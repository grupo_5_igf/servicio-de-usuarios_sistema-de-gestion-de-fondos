<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Estadocivil;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Version;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Estadocivil controller.
 *
 * @Route("estadocivil")
 */
class EstadocivilController extends Controller
{
    /**
     * Lists all estadocivil entities.
     *
     * @Route("/", name="estadocivil_index")
     * @Method("GET")
     */
    public function indexAction()
    {
      $em = $this->getDoctrine();

      $estados = $em->getRepository('AppBundle:Estadocivil')->createQueryBuilder('q')->getQuery()->getArrayResult();

      return new JsonResponse($estados);
    }

    /**
     * Creates a new estadocivil entity.
     *
     * @Route("/new", name="estadocivil_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $estadocivil = new Estadocivil();
        $form = $this->createForm('AppBundle\Form\EstadocivilType', $estadocivil);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($estadocivil);
            $em->flush();

            return $this->redirectToRoute('estadocivil_show', array('idEstadoCivil' => $estadocivil->getIdestadocivil()));
        }

        return $this->render('estadocivil/new.html.twig', array(
            'estadocivil' => $estadocivil,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a estadocivil entity.
     *
     * @Route("/{idEstadoCivil}", name="estadocivil_show")
     * @Method("GET")
     */
    public function showAction(Estadocivil $estadocivil)
    {
        $deleteForm = $this->createDeleteForm($estadocivil);

        return $this->render('estadocivil/show.html.twig', array(
            'estadocivil' => $estadocivil,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing estadocivil entity.
     *
     * @Route("/{idEstadoCivil}/edit", name="estadocivil_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Estadocivil $estadocivil)
    {
        $deleteForm = $this->createDeleteForm($estadocivil);
        $editForm = $this->createForm('AppBundle\Form\EstadocivilType', $estadocivil);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('estadocivil_edit', array('idEstadoCivil' => $estadocivil->getIdestadocivil()));
        }

        return $this->render('estadocivil/edit.html.twig', array(
            'estadocivil' => $estadocivil,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a estadocivil entity.
     *
     * @Route("/{idEstadoCivil}", name="estadocivil_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Estadocivil $estadocivil)
    {
        $form = $this->createDeleteForm($estadocivil);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($estadocivil);
            $em->flush();
        }

        return $this->redirectToRoute('estadocivil_index');
    }

    /**
     * Creates a form to delete a estadocivil entity.
     *
     * @param Estadocivil $estadocivil The estadocivil entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Estadocivil $estadocivil)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('estadocivil_delete', array('idEstadoCivil' => $estadocivil->getIdestadocivil())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
