<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Persona;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Version;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
/**
 * Persona controller.
 *
 * @Route("persona")
 */
class PersonaController extends FOSRestController
{
    /**
     * Lists all persona entities.
     *
     * @Route("/", name="persona_index")
     * @Method("GET")
     */
    public function indexAction()
    {
      $em = $this->getDoctrine();

      $personas = $em->getRepository('AppBundle:Persona')->createQueryBuilder('q')->getQuery()->getArrayResult();

      return new JsonResponse($personas);
    }

    /**
     * Creates a new persona entity.
     *
     * @Route("/new", name="persona_new")
     * @ParamConverter("persona", converter="fos_rest.request_body")
     * @Method("POST")
     */
    public function newAction(Persona $persona)
    {
      $em = $this->getDoctrine()->getManager();
      $persona=$em->merge($persona);
      $em->persist($persona);
      $em->flush();

      $statusCode=200;
      $view=$this->view($persona,$statusCode);

          return  $this->handleView($view);
    }

    /**
     * Finds and displays a persona entity.
     *
     * @Route("/{idPersona}", name="persona_show")
     * @Method("GET")
     */
    public function showAction($idPersona)
    {
      $persona=$this->getDoctrine()->getRepository(Persona::class)->find($idPersona);
        if($persona != null){
          $statusCode=200;


          $view=$this->view($persona,$statusCode);
        return  $this->handleView($view);
      }else{
        throw new HttpException(400, "Empleado no Encontrado.");
      }
    }

    /**
     * Displays a form to edit an existing persona entity.
     *
     * @Route("/{idPersona}/edit", name="persona_edit")
     * @ParamConverter("persona", converter="fos_rest.request_body")
     * @Method("PUT")
     */
    public function editAction($idPersona, Persona $persona)
    {
      $personaR=$this->getDoctrine()->getRepository(Persona::class)->find($idPersona);

      if($personaR != null){

        $personaR->setNombre($persona->getNombre());
        $personaR->setApellido($persona->getApellido());
        $personaR->setDui($persona->getDui());
        $personaR->setNit($persona->getNit());
        $personaR->setDireccion($persona->getDireccion());
        $personaR->setTelefono($persona->getTelefono());

        $em=$this->getDoctrine()->getManager();

        $personaR->setIdEstadoCivil($em->merge($persona->getIdEstadoCivil()));
        $personaR->setIdSexo($em->merge($persona->getIdSexo()));

        $personaR=$em->merge($personaR);
        $em->persist($personaR);
        $em->flush();

        $statusCode=200;
        $view=$this->view($personaR,$statusCode);
      return  $this->handleView($view);
      }else{
          throw new HttpException(400, "Empleado no Encontrado.");
      }
    }

    /**
     * Deletes a persona entity.
     *
     * @Route("/{idPersona}", name="persona_delete")
     * @Method("DELETE")
     */
    public function deleteAction($idPersona)
    {
      $persona=$this->getDoctrine()->getRepository(Persona::class)->find($idPersona);
          if($persona != null){

            $em = $this->getDoctrine()->getManager();
            $em->remove($persona);
            $em->flush();
              return new JsonResponse("Borrado con exito");
            }else{
            throw new HttpException(400, "Empleado no encontrado.");
            }
    }

    /**
     * Creates a form to delete a persona entity.
     *
     * @param Persona $persona The persona entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Persona $persona)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('persona_delete', array('idPersona' => $persona->getIdpersona())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
