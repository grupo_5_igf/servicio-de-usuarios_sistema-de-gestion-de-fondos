<?php
// src/AppBundle/Controller/SecurityController.php

namespace AppBundle\Controller;
use AppBundle\Entity\Usuario;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JWT\Authentication\JWT;
class SecurityController extends FOSRestController
{
    /**
     * @Route("/login", name="login")
     * @Method("POST")
     * @ParamConverter("usuario", converter="fos_rest.request_body")
     */
    public function loginAction(Usuario $usuario)
    {
      $correo=$usuario->getCorreo();
      $pass=$usuario->getContrasenia();
      $user = $this->getDoctrine()->getRepository(Usuario::class)
          ->findOneBy(['correo' => $correo]);
  
      if(!$user) {
          throw $this->createNotFoundException();
      }elseif($user->getContrasenia()!= $pass){
         throw new AuthenticationException('Contraseña no valida.');
      }
            //Segundos*minutos
    $limitTime=60*45;
    $userApiKey = '33b03f9f-cdc8-4f9d-9ded-8fcc7e04c3f5';
    $nonce = base64_encode(substr(md5(uniqid()), 0, 16));
            //Fecha actual
    $created  = date('c');
    $token   = base64_encode("nonce:::" . base64_decode($nonce) .":::User:::" . $user->getCorreo() .":::Password:::" . $user->getContrasenia() .":::TimeCreate:::" . $created .":::TimeLimit:::" . $limitTime . ":::ApiKey:::" . $userApiKey);
      // Return tocken
     $response=$arrayName = array('token' => $token ,'usuario'=>$user);
        $statusCode=200;
      $view=$this->view($response,$statusCode);
        return $view;
       
    }
}