<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Usuario;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Version;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Usuario controller.
 *
 * @Route("usuario")
 */
class UsuarioController extends FOSRestController
{
    /**
     * Lists all usuario entities.
     *
     * @Route("/", name="usuario_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine();

        $usuarios = $em->getRepository('AppBundle:Usuario')->findAll();
        $statusCode=200;
      $view=$this->view($usuarios,$statusCode);
        return $view;
    }

    /**
     * Creates a new usuario entity.
     *
     * @Route("/new", name="usuario_new")
     * @ParamConverter("usuario", converter="fos_rest.request_body")
     * @Method("POST")
     */
    public function newAction(Usuario $usuario)
    {
      $em = $this->getDoctrine()->getManager();
      $usuario->setIdPersona($em->merge($usuario->getIdPersona()));
      $usuario=$em->merge($usuario);


      $em->persist($usuario);
      $em->flush();

      $statusCode=200;
      $view=$this->view($usuario,$statusCode);

              return  $this->handleView($view);
    }

    /**
     * Finds and displays a usuario entity.
     *
     * @Route("/{idUsuario}", name="usuario_show")
     * @Method("GET")
     */
    public function showAction($idUsuario)
    {
      $usuario=$this->getDoctrine()->getRepository(Usuario::class)->find($idUsuario);
            if($usuario != null){
            $statusCode=200;
            $view=$this->view($usuario,$statusCode);
          return  $this->handleView($view);
        }else{
            throw new HttpException(400, "Usuario no encontrado.");
        }
    }

    /**
     * Displays a form to edit an existing usuario entity.
     *
     * @Route("/{idUsuario}/edit", name="usuario_edit")
     * @ParamConverter("usuario", converter="fos_rest.request_body")
     * @Method("PUT")
     */
    public function editAction($idUsuario, Usuario $usuario)
    {
      $usuarioR=$this->getDoctrine()->getRepository(Usuario::class)->find($idUsuario);

      if($usuarioR != null){

      $usuarioR->setCorreo($usuario->getCorreo());
      $usuarioR->setContrasenia($usuario->getContrasenia());
      $usuarioR->setEstado($usuario->getEstado());

      $em=$this->getDoctrine()->getManager();

      $usuarioR->setIdRol($em->merge($usuario->getIdRol()));

        $usuarioR=$em->merge($usuarioR);
        $em->persist($usuarioR);
        $em->flush();

        $statusCode=200;
        $view=$this->view($usuarioR,$statusCode);
        return  $this->handleView($view);
      }else{
          throw new HttpException(400, "Usuario no Encontrado.");
      }
    }

    /**
     * Deletes a usuario entity.
     *
     * @Route("/{idUsuario}", name="usuario_delete")
     * @Method("DELETE")
     */
    public function deleteAction($idUsuario)
    {
      $usuario=$this->getDoctrine()->getRepository(Usuario::class)->find($idUsuario);
              if($usuario != null){

                  $em = $this->getDoctrine()->getManager();
                  $em->remove($usuario);
                  $em->flush();
              return new JsonResponse("Borrado con exito");
            }else{
            throw new HttpException(400, "Usuario no encontrado.");
            }
    }

    /**
     * Creates a form to delete a usuario entity.
     *
     * @param Usuario $usuario The usuario entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Usuario $usuario)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('usuario_delete', array('idUsuario' => $usuario->getIdusuario())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
