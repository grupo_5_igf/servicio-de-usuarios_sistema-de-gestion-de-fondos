<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sexo
 *
 * @ORM\Table(name="sexo", uniqueConstraints={@ORM\UniqueConstraint(name="sexo_pk", columns={"id_sexo"})})
 * @ORM\Entity
 */
class Sexo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_sexo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="sexo_id_sexo_seq", allocationSize=1, initialValue=1)
     */
    private $idSexo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_sexo", type="string", length=10, nullable=true)
     */
    private $nombreSexo;



    /**
     * Get idSexo
     *
     * @return integer
     */
    public function getIdSexo()
    {
        return $this->idSexo;
    }

    public function setidSexo($idSexo){
      $this->idSexo = $idSexo;
      return $this;
    }
    /**
     * Set nombreSexo
     *
     * @param string $nombreSexo
     *
     * @return Sexo
     */
    public function setNombreSexo($nombreSexo)
    {
        $this->nombreSexo = $nombreSexo;

        return $this;
    }

    /**
     * Get nombreSexo
     *
     * @return string
     */
    public function getNombreSexo()
    {
        return $this->nombreSexo;
    }
}
