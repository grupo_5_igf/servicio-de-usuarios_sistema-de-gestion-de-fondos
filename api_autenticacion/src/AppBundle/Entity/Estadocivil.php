<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Estadocivil
 *
 * @ORM\Table(name="estadocivil", uniqueConstraints={@ORM\UniqueConstraint(name="estadocivil_pk", columns={"id_estado_civil"})})
 * @ORM\Entity
 */
class Estadocivil
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_estado_civil", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="estadocivil_id_estado_civil_seq", allocationSize=1, initialValue=1)
     */
    private $idEstadoCivil;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_estado_civil", type="string", length=15, nullable=true)
     */
    private $nombreEstadoCivil;



    /**
     * Get idEstadoCivil
     *
     * @return integer
     */
    public function getIdEstadoCivil()
    {
        return $this->idEstadoCivil;
    }

    public function setidEstadoCivil($idEstadoCivil){
      $this->idEstadoCivil = $idEstadoCivil;
      return $this;
    }

    /**
     * Set nombreEstadoCivil
     *
     * @param string $nombreEstadoCivil
     *
     * @return Estadocivil
     */
    public function setNombreEstadoCivil($nombreEstadoCivil)
    {
        $this->nombreEstadoCivil = $nombreEstadoCivil;

        return $this;
    }

    /**
     * Get nombreEstadoCivil
     *
     * @return string
     */
    public function getNombreEstadoCivil()
    {
        return $this->nombreEstadoCivil;
    }
}
