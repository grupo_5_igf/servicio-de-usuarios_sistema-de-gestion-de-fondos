<?php

/* :estadocivil:edit.html.twig */
class __TwigTemplate_2ba82d55ac3ab54b43c30199260a53b84048b092a55bb3e9232d4e374e591470 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":estadocivil:edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_791a5880a49d11765405761ec8fd72b3dfd995b4c3df6506a5c1935b141a2fc5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_791a5880a49d11765405761ec8fd72b3dfd995b4c3df6506a5c1935b141a2fc5->enter($__internal_791a5880a49d11765405761ec8fd72b3dfd995b4c3df6506a5c1935b141a2fc5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":estadocivil:edit.html.twig"));

        $__internal_2e45b8dbc83364dea540cb174ea9af1ea66f0f4bba9da2698042ecc13c9d1413 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2e45b8dbc83364dea540cb174ea9af1ea66f0f4bba9da2698042ecc13c9d1413->enter($__internal_2e45b8dbc83364dea540cb174ea9af1ea66f0f4bba9da2698042ecc13c9d1413_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":estadocivil:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_791a5880a49d11765405761ec8fd72b3dfd995b4c3df6506a5c1935b141a2fc5->leave($__internal_791a5880a49d11765405761ec8fd72b3dfd995b4c3df6506a5c1935b141a2fc5_prof);

        
        $__internal_2e45b8dbc83364dea540cb174ea9af1ea66f0f4bba9da2698042ecc13c9d1413->leave($__internal_2e45b8dbc83364dea540cb174ea9af1ea66f0f4bba9da2698042ecc13c9d1413_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_5a1ea0d3b01df79bfaf1f336276075159e26244b0ecbfd55ed8487d9872e1696 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5a1ea0d3b01df79bfaf1f336276075159e26244b0ecbfd55ed8487d9872e1696->enter($__internal_5a1ea0d3b01df79bfaf1f336276075159e26244b0ecbfd55ed8487d9872e1696_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_a7a5a4ef539c45bf427d157a2010157be3577c9adbc4a6d7c378db52e1be4b8b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a7a5a4ef539c45bf427d157a2010157be3577c9adbc4a6d7c378db52e1be4b8b->enter($__internal_a7a5a4ef539c45bf427d157a2010157be3577c9adbc4a6d7c378db52e1be4b8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Estadocivil edit</h1>

    ";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Edit\" />
    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("estadocivil_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            ";
        // line 16
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 18
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_a7a5a4ef539c45bf427d157a2010157be3577c9adbc4a6d7c378db52e1be4b8b->leave($__internal_a7a5a4ef539c45bf427d157a2010157be3577c9adbc4a6d7c378db52e1be4b8b_prof);

        
        $__internal_5a1ea0d3b01df79bfaf1f336276075159e26244b0ecbfd55ed8487d9872e1696->leave($__internal_5a1ea0d3b01df79bfaf1f336276075159e26244b0ecbfd55ed8487d9872e1696_prof);

    }

    public function getTemplateName()
    {
        return ":estadocivil:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 18,  75 => 16,  69 => 13,  62 => 9,  57 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Estadocivil edit</h1>

    {{ form_start(edit_form) }}
        {{ form_widget(edit_form) }}
        <input type=\"submit\" value=\"Edit\" />
    {{ form_end(edit_form) }}

    <ul>
        <li>
            <a href=\"{{ path('estadocivil_index') }}\">Back to the list</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", ":estadocivil:edit.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/app/Resources/views/estadocivil/edit.html.twig");
    }
}
