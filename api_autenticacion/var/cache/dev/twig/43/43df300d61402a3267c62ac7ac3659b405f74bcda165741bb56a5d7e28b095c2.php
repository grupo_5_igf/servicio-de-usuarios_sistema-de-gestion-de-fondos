<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_5f485990e19975f2fc49879b76b3dbef14e71bbc44366a68a96b2e5ff040571b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_71f0a8d154bf1c63e05274a8358b4fea6997a486f3129780bb0637de60c177e9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_71f0a8d154bf1c63e05274a8358b4fea6997a486f3129780bb0637de60c177e9->enter($__internal_71f0a8d154bf1c63e05274a8358b4fea6997a486f3129780bb0637de60c177e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        $__internal_943886cfb7079882f21514f9d41b829818af19880b0de3a4f19b979e17f875d6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_943886cfb7079882f21514f9d41b829818af19880b0de3a4f19b979e17f875d6->enter($__internal_943886cfb7079882f21514f9d41b829818af19880b0de3a4f19b979e17f875d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_71f0a8d154bf1c63e05274a8358b4fea6997a486f3129780bb0637de60c177e9->leave($__internal_71f0a8d154bf1c63e05274a8358b4fea6997a486f3129780bb0637de60c177e9_prof);

        
        $__internal_943886cfb7079882f21514f9d41b829818af19880b0de3a4f19b979e17f875d6->leave($__internal_943886cfb7079882f21514f9d41b829818af19880b0de3a4f19b979e17f875d6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
", "@Framework/Form/form_rest.html.php", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rest.html.php");
    }
}
