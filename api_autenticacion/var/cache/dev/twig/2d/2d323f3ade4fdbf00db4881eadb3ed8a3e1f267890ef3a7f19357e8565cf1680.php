<?php

/* :rol:new.html.twig */
class __TwigTemplate_0f315446e30f4810214fd15209b31e3d83d6803dda099b0e6257c6bb5b341c6a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":rol:new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aede249d52628d8b781790b7e9071dad87cd058d10e2fc3e6bb2d5d87f1ac476 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aede249d52628d8b781790b7e9071dad87cd058d10e2fc3e6bb2d5d87f1ac476->enter($__internal_aede249d52628d8b781790b7e9071dad87cd058d10e2fc3e6bb2d5d87f1ac476_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":rol:new.html.twig"));

        $__internal_6c8ee84232be3c5caeaa463876394d975b1275cc490ce5bdd7e0dfdea493ac24 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6c8ee84232be3c5caeaa463876394d975b1275cc490ce5bdd7e0dfdea493ac24->enter($__internal_6c8ee84232be3c5caeaa463876394d975b1275cc490ce5bdd7e0dfdea493ac24_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":rol:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_aede249d52628d8b781790b7e9071dad87cd058d10e2fc3e6bb2d5d87f1ac476->leave($__internal_aede249d52628d8b781790b7e9071dad87cd058d10e2fc3e6bb2d5d87f1ac476_prof);

        
        $__internal_6c8ee84232be3c5caeaa463876394d975b1275cc490ce5bdd7e0dfdea493ac24->leave($__internal_6c8ee84232be3c5caeaa463876394d975b1275cc490ce5bdd7e0dfdea493ac24_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_ea18fd2888cda232c58327547773e8c9f8dfd10eabc0e93e636dedaf8ce04c39 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ea18fd2888cda232c58327547773e8c9f8dfd10eabc0e93e636dedaf8ce04c39->enter($__internal_ea18fd2888cda232c58327547773e8c9f8dfd10eabc0e93e636dedaf8ce04c39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_80fb4331cdc5c7e2a1463a86426daccde4988801b87a61ccdefac45188cfa781 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_80fb4331cdc5c7e2a1463a86426daccde4988801b87a61ccdefac45188cfa781->enter($__internal_80fb4331cdc5c7e2a1463a86426daccde4988801b87a61ccdefac45188cfa781_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Rol creation</h1>

    ";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" />
    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("rol_index");
        echo "\">Back to the list</a>
        </li>
    </ul>
";
        
        $__internal_80fb4331cdc5c7e2a1463a86426daccde4988801b87a61ccdefac45188cfa781->leave($__internal_80fb4331cdc5c7e2a1463a86426daccde4988801b87a61ccdefac45188cfa781_prof);

        
        $__internal_ea18fd2888cda232c58327547773e8c9f8dfd10eabc0e93e636dedaf8ce04c39->leave($__internal_ea18fd2888cda232c58327547773e8c9f8dfd10eabc0e93e636dedaf8ce04c39_prof);

    }

    public function getTemplateName()
    {
        return ":rol:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 13,  62 => 9,  57 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Rol creation</h1>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" />
    {{ form_end(form) }}

    <ul>
        <li>
            <a href=\"{{ path('rol_index') }}\">Back to the list</a>
        </li>
    </ul>
{% endblock %}
", ":rol:new.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/app/Resources/views/rol/new.html.twig");
    }
}
