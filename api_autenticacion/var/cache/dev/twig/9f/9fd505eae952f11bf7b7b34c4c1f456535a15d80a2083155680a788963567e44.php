<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_5a3f6cf6086d484b923836b0a12f513cb56c7628150a5bb964f028a5858c76b5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f135309b4d4d6831fba3eaa39e869a35b56fc410b12d77f8d00a073760a8b5c9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f135309b4d4d6831fba3eaa39e869a35b56fc410b12d77f8d00a073760a8b5c9->enter($__internal_f135309b4d4d6831fba3eaa39e869a35b56fc410b12d77f8d00a073760a8b5c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        $__internal_de370b974a2d2a31a11566229ff6d27c7f585abbfb7b23fc75b77853e05422c3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_de370b974a2d2a31a11566229ff6d27c7f585abbfb7b23fc75b77853e05422c3->enter($__internal_de370b974a2d2a31a11566229ff6d27c7f585abbfb7b23fc75b77853e05422c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_f135309b4d4d6831fba3eaa39e869a35b56fc410b12d77f8d00a073760a8b5c9->leave($__internal_f135309b4d4d6831fba3eaa39e869a35b56fc410b12d77f8d00a073760a8b5c9_prof);

        
        $__internal_de370b974a2d2a31a11566229ff6d27c7f585abbfb7b23fc75b77853e05422c3->leave($__internal_de370b974a2d2a31a11566229ff6d27c7f585abbfb7b23fc75b77853e05422c3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
", "@Framework/Form/integer_widget.html.php", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/integer_widget.html.php");
    }
}
