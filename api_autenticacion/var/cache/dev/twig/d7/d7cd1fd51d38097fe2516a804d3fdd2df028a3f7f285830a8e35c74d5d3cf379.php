<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_3caf6e65f122c85d80570a97d8460867e5eab2af60bd6b4a0da35d6c33fadfda extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e89e85e666652e3d94095a49020fc8a3de6f2492003a8a3c935d938300a2bc81 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e89e85e666652e3d94095a49020fc8a3de6f2492003a8a3c935d938300a2bc81->enter($__internal_e89e85e666652e3d94095a49020fc8a3de6f2492003a8a3c935d938300a2bc81_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        $__internal_f8192917c3d81478ff126588073ad453e5475f6da8b40df2ba9486f9ddc0df33 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f8192917c3d81478ff126588073ad453e5475f6da8b40df2ba9486f9ddc0df33->enter($__internal_f8192917c3d81478ff126588073ad453e5475f6da8b40df2ba9486f9ddc0df33_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_e89e85e666652e3d94095a49020fc8a3de6f2492003a8a3c935d938300a2bc81->leave($__internal_e89e85e666652e3d94095a49020fc8a3de6f2492003a8a3c935d938300a2bc81_prof);

        
        $__internal_f8192917c3d81478ff126588073ad453e5475f6da8b40df2ba9486f9ddc0df33->leave($__internal_f8192917c3d81478ff126588073ad453e5475f6da8b40df2ba9486f9ddc0df33_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/radio_widget.html.php", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/radio_widget.html.php");
    }
}
