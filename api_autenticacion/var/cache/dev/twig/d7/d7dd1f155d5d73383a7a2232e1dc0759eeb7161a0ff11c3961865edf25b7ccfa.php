<?php

/* :persona:edit.html.twig */
class __TwigTemplate_566b7b9782d1fa1bf7e4d74131e1681a437034c667ef0139bd11729cc0b8e733 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":persona:edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_91a0d4699afca954367a8852af12198f04822bd0cf393d9826049da3d3e730d3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_91a0d4699afca954367a8852af12198f04822bd0cf393d9826049da3d3e730d3->enter($__internal_91a0d4699afca954367a8852af12198f04822bd0cf393d9826049da3d3e730d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":persona:edit.html.twig"));

        $__internal_08ef8c4a2cbbbba105d5948981469e0ab9dd2e6c990edec455d7fb2dc49cf66e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_08ef8c4a2cbbbba105d5948981469e0ab9dd2e6c990edec455d7fb2dc49cf66e->enter($__internal_08ef8c4a2cbbbba105d5948981469e0ab9dd2e6c990edec455d7fb2dc49cf66e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":persona:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_91a0d4699afca954367a8852af12198f04822bd0cf393d9826049da3d3e730d3->leave($__internal_91a0d4699afca954367a8852af12198f04822bd0cf393d9826049da3d3e730d3_prof);

        
        $__internal_08ef8c4a2cbbbba105d5948981469e0ab9dd2e6c990edec455d7fb2dc49cf66e->leave($__internal_08ef8c4a2cbbbba105d5948981469e0ab9dd2e6c990edec455d7fb2dc49cf66e_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_5fb1a4b9405928940d9480c43c24c64ba8ad8b59f65a12b608693a9a2c9a518e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5fb1a4b9405928940d9480c43c24c64ba8ad8b59f65a12b608693a9a2c9a518e->enter($__internal_5fb1a4b9405928940d9480c43c24c64ba8ad8b59f65a12b608693a9a2c9a518e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_a8eca551516aa86f1a6b31b3459666f10aed34aeda4269b860bd2b76817fc39e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a8eca551516aa86f1a6b31b3459666f10aed34aeda4269b860bd2b76817fc39e->enter($__internal_a8eca551516aa86f1a6b31b3459666f10aed34aeda4269b860bd2b76817fc39e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Persona edit</h1>

    ";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Edit\" />
    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("persona_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            ";
        // line 16
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 18
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_a8eca551516aa86f1a6b31b3459666f10aed34aeda4269b860bd2b76817fc39e->leave($__internal_a8eca551516aa86f1a6b31b3459666f10aed34aeda4269b860bd2b76817fc39e_prof);

        
        $__internal_5fb1a4b9405928940d9480c43c24c64ba8ad8b59f65a12b608693a9a2c9a518e->leave($__internal_5fb1a4b9405928940d9480c43c24c64ba8ad8b59f65a12b608693a9a2c9a518e_prof);

    }

    public function getTemplateName()
    {
        return ":persona:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 18,  75 => 16,  69 => 13,  62 => 9,  57 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Persona edit</h1>

    {{ form_start(edit_form) }}
        {{ form_widget(edit_form) }}
        <input type=\"submit\" value=\"Edit\" />
    {{ form_end(edit_form) }}

    <ul>
        <li>
            <a href=\"{{ path('persona_index') }}\">Back to the list</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", ":persona:edit.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/app/Resources/views/persona/edit.html.twig");
    }
}
