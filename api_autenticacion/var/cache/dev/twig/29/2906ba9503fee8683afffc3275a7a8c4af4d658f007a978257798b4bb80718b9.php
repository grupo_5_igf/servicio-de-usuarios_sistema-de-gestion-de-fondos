<?php

/* @Framework/Form/choice_options.html.php */
class __TwigTemplate_2c98d7dda513a28e40954cf0fe4f994038abd57bf1c7e44dd11bb7e023e5e1e0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0fb40965ecf0259a7d6aca7139c25180a04e88b966ce248e3b0afb6f695d2a5b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0fb40965ecf0259a7d6aca7139c25180a04e88b966ce248e3b0afb6f695d2a5b->enter($__internal_0fb40965ecf0259a7d6aca7139c25180a04e88b966ce248e3b0afb6f695d2a5b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        $__internal_330badc5917401a644929f8ea4e09b5468082db07a1f8354e231bb1773f65fda = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_330badc5917401a644929f8ea4e09b5468082db07a1f8354e231bb1773f65fda->enter($__internal_330badc5917401a644929f8ea4e09b5468082db07a1f8354e231bb1773f65fda_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
";
        
        $__internal_0fb40965ecf0259a7d6aca7139c25180a04e88b966ce248e3b0afb6f695d2a5b->leave($__internal_0fb40965ecf0259a7d6aca7139c25180a04e88b966ce248e3b0afb6f695d2a5b_prof);

        
        $__internal_330badc5917401a644929f8ea4e09b5468082db07a1f8354e231bb1773f65fda->leave($__internal_330badc5917401a644929f8ea4e09b5468082db07a1f8354e231bb1773f65fda_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_options.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
", "@Framework/Form/choice_options.html.php", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_options.html.php");
    }
}
