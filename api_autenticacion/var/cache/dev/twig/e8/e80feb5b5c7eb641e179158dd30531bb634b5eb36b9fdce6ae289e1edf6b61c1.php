<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_aa83498be161242a9f91d6a22df145b34e65dacd1633e3e28a2baeb8081b4468 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dd68232bbbc7ed492550501bd7bb7f817816964b64c36fcffc98fda76cb911f3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dd68232bbbc7ed492550501bd7bb7f817816964b64c36fcffc98fda76cb911f3->enter($__internal_dd68232bbbc7ed492550501bd7bb7f817816964b64c36fcffc98fda76cb911f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        $__internal_065085761a8b9b569524cacfc564ddfba89f89c9f9e40f5fa4d8b0c4375fd27a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_065085761a8b9b569524cacfc564ddfba89f89c9f9e40f5fa4d8b0c4375fd27a->enter($__internal_065085761a8b9b569524cacfc564ddfba89f89c9f9e40f5fa4d8b0c4375fd27a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
";
        
        $__internal_dd68232bbbc7ed492550501bd7bb7f817816964b64c36fcffc98fda76cb911f3->leave($__internal_dd68232bbbc7ed492550501bd7bb7f817816964b64c36fcffc98fda76cb911f3_prof);

        
        $__internal_065085761a8b9b569524cacfc564ddfba89f89c9f9e40f5fa4d8b0c4375fd27a->leave($__internal_065085761a8b9b569524cacfc564ddfba89f89c9f9e40f5fa4d8b0c4375fd27a_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/exception.xml.twig', { exception: exception }) }}
", "TwigBundle:Exception:exception.atom.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.atom.twig");
    }
}
