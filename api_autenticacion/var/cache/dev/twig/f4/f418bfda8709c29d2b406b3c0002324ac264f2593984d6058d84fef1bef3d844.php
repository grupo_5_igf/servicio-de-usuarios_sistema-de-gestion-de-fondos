<?php

/* :estadocivil:index.html.twig */
class __TwigTemplate_040d3e2a2a805c9fce99f32c11db153bf6640121420cd0d1ae1aebb5245f5040 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":estadocivil:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1233f695d26b7be28c10ed32bae234a02f152fb7d6765b49f32c407082b2dcae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1233f695d26b7be28c10ed32bae234a02f152fb7d6765b49f32c407082b2dcae->enter($__internal_1233f695d26b7be28c10ed32bae234a02f152fb7d6765b49f32c407082b2dcae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":estadocivil:index.html.twig"));

        $__internal_8c2dbb0e55b0d657c855e2e895612c613ae6f3981c621ace82b325cf4b4cf06c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8c2dbb0e55b0d657c855e2e895612c613ae6f3981c621ace82b325cf4b4cf06c->enter($__internal_8c2dbb0e55b0d657c855e2e895612c613ae6f3981c621ace82b325cf4b4cf06c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":estadocivil:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1233f695d26b7be28c10ed32bae234a02f152fb7d6765b49f32c407082b2dcae->leave($__internal_1233f695d26b7be28c10ed32bae234a02f152fb7d6765b49f32c407082b2dcae_prof);

        
        $__internal_8c2dbb0e55b0d657c855e2e895612c613ae6f3981c621ace82b325cf4b4cf06c->leave($__internal_8c2dbb0e55b0d657c855e2e895612c613ae6f3981c621ace82b325cf4b4cf06c_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_70addea449882ad21993d1af8286808ec94b384ba4dc9a406606902b09e9d25a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_70addea449882ad21993d1af8286808ec94b384ba4dc9a406606902b09e9d25a->enter($__internal_70addea449882ad21993d1af8286808ec94b384ba4dc9a406606902b09e9d25a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_f7352b7231e3993f72bef92f161d812864fec7b3b0aed4085a23a3c12ed39056 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f7352b7231e3993f72bef92f161d812864fec7b3b0aed4085a23a3c12ed39056->enter($__internal_f7352b7231e3993f72bef92f161d812864fec7b3b0aed4085a23a3c12ed39056_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Estadocivils list</h1>

    <table>
        <thead>
            <tr>
                <th>Idestadocivil</th>
                <th>Nombreestadocivil</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["estadocivils"] ?? $this->getContext($context, "estadocivils")));
        foreach ($context['_seq'] as $context["_key"] => $context["estadocivil"]) {
            // line 16
            echo "            <tr>
                <td><a href=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("estadocivil_show", array("idEstadoCivil" => $this->getAttribute($context["estadocivil"], "idEstadoCivil", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["estadocivil"], "idEstadoCivil", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["estadocivil"], "nombreEstadoCivil", array()), "html", null, true);
            echo "</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("estadocivil_show", array("idEstadoCivil" => $this->getAttribute($context["estadocivil"], "idEstadoCivil", array()))), "html", null, true);
            echo "\">show</a>
                        </li>
                        <li>
                            <a href=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("estadocivil_edit", array("idEstadoCivil" => $this->getAttribute($context["estadocivil"], "idEstadoCivil", array()))), "html", null, true);
            echo "\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['estadocivil'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 36
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("estadocivil_new");
        echo "\">Create a new estadocivil</a>
        </li>
    </ul>
";
        
        $__internal_f7352b7231e3993f72bef92f161d812864fec7b3b0aed4085a23a3c12ed39056->leave($__internal_f7352b7231e3993f72bef92f161d812864fec7b3b0aed4085a23a3c12ed39056_prof);

        
        $__internal_70addea449882ad21993d1af8286808ec94b384ba4dc9a406606902b09e9d25a->leave($__internal_70addea449882ad21993d1af8286808ec94b384ba4dc9a406606902b09e9d25a_prof);

    }

    public function getTemplateName()
    {
        return ":estadocivil:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 36,  100 => 31,  88 => 25,  82 => 22,  75 => 18,  69 => 17,  66 => 16,  62 => 15,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Estadocivils list</h1>

    <table>
        <thead>
            <tr>
                <th>Idestadocivil</th>
                <th>Nombreestadocivil</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for estadocivil in estadocivils %}
            <tr>
                <td><a href=\"{{ path('estadocivil_show', { 'idEstadoCivil': estadocivil.idEstadoCivil }) }}\">{{ estadocivil.idEstadoCivil }}</a></td>
                <td>{{ estadocivil.nombreEstadoCivil }}</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"{{ path('estadocivil_show', { 'idEstadoCivil': estadocivil.idEstadoCivil }) }}\">show</a>
                        </li>
                        <li>
                            <a href=\"{{ path('estadocivil_edit', { 'idEstadoCivil': estadocivil.idEstadoCivil }) }}\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('estadocivil_new') }}\">Create a new estadocivil</a>
        </li>
    </ul>
{% endblock %}
", ":estadocivil:index.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/app/Resources/views/estadocivil/index.html.twig");
    }
}
