<?php

/* :sexo:edit.html.twig */
class __TwigTemplate_a83e27cbe0ef744331475f8caa7c07d489c945c1892f8b42c98e118eca14f11e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":sexo:edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2d84b9066bd14613beb20bf11bcb2f8f3dcd8b004f855151e2997ffb2e9bb703 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2d84b9066bd14613beb20bf11bcb2f8f3dcd8b004f855151e2997ffb2e9bb703->enter($__internal_2d84b9066bd14613beb20bf11bcb2f8f3dcd8b004f855151e2997ffb2e9bb703_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":sexo:edit.html.twig"));

        $__internal_ad76a9ee9c3ef423272c824d23bd1f6535d625d5bfb7b6a9782d50ee5399234b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ad76a9ee9c3ef423272c824d23bd1f6535d625d5bfb7b6a9782d50ee5399234b->enter($__internal_ad76a9ee9c3ef423272c824d23bd1f6535d625d5bfb7b6a9782d50ee5399234b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":sexo:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2d84b9066bd14613beb20bf11bcb2f8f3dcd8b004f855151e2997ffb2e9bb703->leave($__internal_2d84b9066bd14613beb20bf11bcb2f8f3dcd8b004f855151e2997ffb2e9bb703_prof);

        
        $__internal_ad76a9ee9c3ef423272c824d23bd1f6535d625d5bfb7b6a9782d50ee5399234b->leave($__internal_ad76a9ee9c3ef423272c824d23bd1f6535d625d5bfb7b6a9782d50ee5399234b_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_0fccdf8ba41599c3af64c4ca52675bcfcfb467026654cb3f9ff43940e77228d5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0fccdf8ba41599c3af64c4ca52675bcfcfb467026654cb3f9ff43940e77228d5->enter($__internal_0fccdf8ba41599c3af64c4ca52675bcfcfb467026654cb3f9ff43940e77228d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_492fb1ba3c92a3313bbd465901f6302c1da1d405cd142228b13984cf30f381af = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_492fb1ba3c92a3313bbd465901f6302c1da1d405cd142228b13984cf30f381af->enter($__internal_492fb1ba3c92a3313bbd465901f6302c1da1d405cd142228b13984cf30f381af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Sexo edit</h1>

    ";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Edit\" />
    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("sexo_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            ";
        // line 16
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 18
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_492fb1ba3c92a3313bbd465901f6302c1da1d405cd142228b13984cf30f381af->leave($__internal_492fb1ba3c92a3313bbd465901f6302c1da1d405cd142228b13984cf30f381af_prof);

        
        $__internal_0fccdf8ba41599c3af64c4ca52675bcfcfb467026654cb3f9ff43940e77228d5->leave($__internal_0fccdf8ba41599c3af64c4ca52675bcfcfb467026654cb3f9ff43940e77228d5_prof);

    }

    public function getTemplateName()
    {
        return ":sexo:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 18,  75 => 16,  69 => 13,  62 => 9,  57 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Sexo edit</h1>

    {{ form_start(edit_form) }}
        {{ form_widget(edit_form) }}
        <input type=\"submit\" value=\"Edit\" />
    {{ form_end(edit_form) }}

    <ul>
        <li>
            <a href=\"{{ path('sexo_index') }}\">Back to the list</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", ":sexo:edit.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/app/Resources/views/sexo/edit.html.twig");
    }
}
