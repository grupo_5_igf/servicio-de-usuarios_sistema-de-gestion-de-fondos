<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_fd9884ea6e69926d08e06aa018a95bc85a683fa90791ffcd74a6d168addbfc0c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0467751cee7acfbe664503daa32c91c9ae62983954e69bbabb4ec63ca844b013 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0467751cee7acfbe664503daa32c91c9ae62983954e69bbabb4ec63ca844b013->enter($__internal_0467751cee7acfbe664503daa32c91c9ae62983954e69bbabb4ec63ca844b013_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        $__internal_d4a2e2dd6c2a2da51303afa68bc2faf3e80d2fe9764d9fd5a8adffb072a0441d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d4a2e2dd6c2a2da51303afa68bc2faf3e80d2fe9764d9fd5a8adffb072a0441d->enter($__internal_d4a2e2dd6c2a2da51303afa68bc2faf3e80d2fe9764d9fd5a8adffb072a0441d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_0467751cee7acfbe664503daa32c91c9ae62983954e69bbabb4ec63ca844b013->leave($__internal_0467751cee7acfbe664503daa32c91c9ae62983954e69bbabb4ec63ca844b013_prof);

        
        $__internal_d4a2e2dd6c2a2da51303afa68bc2faf3e80d2fe9764d9fd5a8adffb072a0441d->leave($__internal_d4a2e2dd6c2a2da51303afa68bc2faf3e80d2fe9764d9fd5a8adffb072a0441d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
", "@Framework/Form/form_end.html.php", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_end.html.php");
    }
}
