<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_2911652c367fa8c463fb5e758f16f977e427495afe595ef7f817423d0a9174a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ebef6876c2cec51bf18cc074b149fc3033c69da35b255c6c1d100b9f708f2f0f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ebef6876c2cec51bf18cc074b149fc3033c69da35b255c6c1d100b9f708f2f0f->enter($__internal_ebef6876c2cec51bf18cc074b149fc3033c69da35b255c6c1d100b9f708f2f0f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        $__internal_499cef012bdf4d05b69726d53ce0f60f6d64a375f0772ac57ff5f6aec2dc26a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_499cef012bdf4d05b69726d53ce0f60f6d64a375f0772ac57ff5f6aec2dc26a8->enter($__internal_499cef012bdf4d05b69726d53ce0f60f6d64a375f0772ac57ff5f6aec2dc26a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_ebef6876c2cec51bf18cc074b149fc3033c69da35b255c6c1d100b9f708f2f0f->leave($__internal_ebef6876c2cec51bf18cc074b149fc3033c69da35b255c6c1d100b9f708f2f0f_prof);

        
        $__internal_499cef012bdf4d05b69726d53ce0f60f6d64a375f0772ac57ff5f6aec2dc26a8->leave($__internal_499cef012bdf4d05b69726d53ce0f60f6d64a375f0772ac57ff5f6aec2dc26a8_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "TwigBundle:Exception:error.js.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.js.twig");
    }
}
