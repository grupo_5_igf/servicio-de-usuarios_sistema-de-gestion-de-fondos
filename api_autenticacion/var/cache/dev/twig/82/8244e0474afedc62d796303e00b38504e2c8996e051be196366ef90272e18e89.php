<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_103e10cc104c0b40daf4b4fe99961f77bd9645f170ff7cb7bad7d4b8e86ad26c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_969b0baae01a09840b77302b7705d6a3de4f40c3e52960c0378fa1051185dcd0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_969b0baae01a09840b77302b7705d6a3de4f40c3e52960c0378fa1051185dcd0->enter($__internal_969b0baae01a09840b77302b7705d6a3de4f40c3e52960c0378fa1051185dcd0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        $__internal_35b680e7508959446b49c4178105e63e1073eb10dab5aac1b0f4b0787f2175e6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_35b680e7508959446b49c4178105e63e1073eb10dab5aac1b0f4b0787f2175e6->enter($__internal_35b680e7508959446b49c4178105e63e1073eb10dab5aac1b0f4b0787f2175e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_969b0baae01a09840b77302b7705d6a3de4f40c3e52960c0378fa1051185dcd0->leave($__internal_969b0baae01a09840b77302b7705d6a3de4f40c3e52960c0378fa1051185dcd0_prof);

        
        $__internal_35b680e7508959446b49c4178105e63e1073eb10dab5aac1b0f4b0787f2175e6->leave($__internal_35b680e7508959446b49c4178105e63e1073eb10dab5aac1b0f4b0787f2175e6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
", "@Framework/Form/form_errors.html.php", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_errors.html.php");
    }
}
