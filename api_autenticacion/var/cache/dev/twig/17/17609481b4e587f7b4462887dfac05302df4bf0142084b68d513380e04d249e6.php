<?php

/* :persona:index.html.twig */
class __TwigTemplate_655669ebe4b73d047ead35c0e1c6fdb55af81fd1170ecf13fed7f202a0833a55 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":persona:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a99f0a6dd2e5409dc2680dead05ee674e02d577da5e64be45ad644a758b40124 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a99f0a6dd2e5409dc2680dead05ee674e02d577da5e64be45ad644a758b40124->enter($__internal_a99f0a6dd2e5409dc2680dead05ee674e02d577da5e64be45ad644a758b40124_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":persona:index.html.twig"));

        $__internal_10012823c300276b5ebb62db0cfa4b2a50622d092d833698271b6ff516eda2f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_10012823c300276b5ebb62db0cfa4b2a50622d092d833698271b6ff516eda2f0->enter($__internal_10012823c300276b5ebb62db0cfa4b2a50622d092d833698271b6ff516eda2f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":persona:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a99f0a6dd2e5409dc2680dead05ee674e02d577da5e64be45ad644a758b40124->leave($__internal_a99f0a6dd2e5409dc2680dead05ee674e02d577da5e64be45ad644a758b40124_prof);

        
        $__internal_10012823c300276b5ebb62db0cfa4b2a50622d092d833698271b6ff516eda2f0->leave($__internal_10012823c300276b5ebb62db0cfa4b2a50622d092d833698271b6ff516eda2f0_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_5bf15df491adb600131d28f1a2ca9d44183c2e8714e5dc6cca6224653ae6a71c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5bf15df491adb600131d28f1a2ca9d44183c2e8714e5dc6cca6224653ae6a71c->enter($__internal_5bf15df491adb600131d28f1a2ca9d44183c2e8714e5dc6cca6224653ae6a71c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_bf81b280c73d1fa26ad139f0eafd7928cd4ece625119d4c65a85b5ca8f9f52f2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bf81b280c73d1fa26ad139f0eafd7928cd4ece625119d4c65a85b5ca8f9f52f2->enter($__internal_bf81b280c73d1fa26ad139f0eafd7928cd4ece625119d4c65a85b5ca8f9f52f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Personas list</h1>

    <table>
        <thead>
            <tr>
                <th>Idpersona</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Dui</th>
                <th>Nit</th>
                <th>Direccion</th>
                <th>Telefono</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["personas"] ?? $this->getContext($context, "personas")));
        foreach ($context['_seq'] as $context["_key"] => $context["persona"]) {
            // line 21
            echo "            <tr>
                <td><a href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("persona_show", array("idPersona" => $this->getAttribute($context["persona"], "idPersona", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["persona"], "idPersona", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["persona"], "nombre", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["persona"], "apellido", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["persona"], "dui", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["persona"], "nit", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["persona"], "direccion", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($context["persona"], "telefono", array()), "html", null, true);
            echo "</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"";
            // line 32
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("persona_show", array("idPersona" => $this->getAttribute($context["persona"], "idPersona", array()))), "html", null, true);
            echo "\">show</a>
                        </li>
                        <li>
                            <a href=\"";
            // line 35
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("persona_edit", array("idPersona" => $this->getAttribute($context["persona"], "idPersona", array()))), "html", null, true);
            echo "\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['persona'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 46
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("persona_new");
        echo "\">Create a new persona</a>
        </li>
    </ul>
";
        
        $__internal_bf81b280c73d1fa26ad139f0eafd7928cd4ece625119d4c65a85b5ca8f9f52f2->leave($__internal_bf81b280c73d1fa26ad139f0eafd7928cd4ece625119d4c65a85b5ca8f9f52f2_prof);

        
        $__internal_5bf15df491adb600131d28f1a2ca9d44183c2e8714e5dc6cca6224653ae6a71c->leave($__internal_5bf15df491adb600131d28f1a2ca9d44183c2e8714e5dc6cca6224653ae6a71c_prof);

    }

    public function getTemplateName()
    {
        return ":persona:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 46,  125 => 41,  113 => 35,  107 => 32,  100 => 28,  96 => 27,  92 => 26,  88 => 25,  84 => 24,  80 => 23,  74 => 22,  71 => 21,  67 => 20,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Personas list</h1>

    <table>
        <thead>
            <tr>
                <th>Idpersona</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Dui</th>
                <th>Nit</th>
                <th>Direccion</th>
                <th>Telefono</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for persona in personas %}
            <tr>
                <td><a href=\"{{ path('persona_show', { 'idPersona': persona.idPersona }) }}\">{{ persona.idPersona }}</a></td>
                <td>{{ persona.nombre }}</td>
                <td>{{ persona.apellido }}</td>
                <td>{{ persona.dui }}</td>
                <td>{{ persona.nit }}</td>
                <td>{{ persona.direccion }}</td>
                <td>{{ persona.telefono }}</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"{{ path('persona_show', { 'idPersona': persona.idPersona }) }}\">show</a>
                        </li>
                        <li>
                            <a href=\"{{ path('persona_edit', { 'idPersona': persona.idPersona }) }}\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('persona_new') }}\">Create a new persona</a>
        </li>
    </ul>
{% endblock %}
", ":persona:index.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/app/Resources/views/persona/index.html.twig");
    }
}
