<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_23d2a24170420de7d4c3e5aed4a2f763687bcaa704969614733b72bfc477a56d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_edfc548ebcc3fbd6effb5915a77f6b442dcea83847c99b89153b301866978e7c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_edfc548ebcc3fbd6effb5915a77f6b442dcea83847c99b89153b301866978e7c->enter($__internal_edfc548ebcc3fbd6effb5915a77f6b442dcea83847c99b89153b301866978e7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        $__internal_dd2d9214916114ff4b87c53c8272993f33de807201503492462090f4e0f519a6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dd2d9214916114ff4b87c53c8272993f33de807201503492462090f4e0f519a6->enter($__internal_dd2d9214916114ff4b87c53c8272993f33de807201503492462090f4e0f519a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_edfc548ebcc3fbd6effb5915a77f6b442dcea83847c99b89153b301866978e7c->leave($__internal_edfc548ebcc3fbd6effb5915a77f6b442dcea83847c99b89153b301866978e7c_prof);

        
        $__internal_dd2d9214916114ff4b87c53c8272993f33de807201503492462090f4e0f519a6->leave($__internal_dd2d9214916114ff4b87c53c8272993f33de807201503492462090f4e0f519a6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
", "@Framework/Form/hidden_widget.html.php", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_widget.html.php");
    }
}
