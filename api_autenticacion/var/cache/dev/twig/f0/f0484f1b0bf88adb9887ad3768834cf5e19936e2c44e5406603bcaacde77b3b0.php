<?php

/* :sexo:index.html.twig */
class __TwigTemplate_9e555fc5f1d6f5fd41025d022ac9ca28f286e1eb3f1c079965dcaf43022862f0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":sexo:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1b1e94691871d07bf033a777119249c68fd949d0d48711978d4594898b5f9b62 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1b1e94691871d07bf033a777119249c68fd949d0d48711978d4594898b5f9b62->enter($__internal_1b1e94691871d07bf033a777119249c68fd949d0d48711978d4594898b5f9b62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":sexo:index.html.twig"));

        $__internal_92ca32c89eaf2f95ae2c04dc7c653f93eb053d04b3c1773a9e43f9a72ed1a64a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_92ca32c89eaf2f95ae2c04dc7c653f93eb053d04b3c1773a9e43f9a72ed1a64a->enter($__internal_92ca32c89eaf2f95ae2c04dc7c653f93eb053d04b3c1773a9e43f9a72ed1a64a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":sexo:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1b1e94691871d07bf033a777119249c68fd949d0d48711978d4594898b5f9b62->leave($__internal_1b1e94691871d07bf033a777119249c68fd949d0d48711978d4594898b5f9b62_prof);

        
        $__internal_92ca32c89eaf2f95ae2c04dc7c653f93eb053d04b3c1773a9e43f9a72ed1a64a->leave($__internal_92ca32c89eaf2f95ae2c04dc7c653f93eb053d04b3c1773a9e43f9a72ed1a64a_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_00eb00301df8460cac3ef6487a20cca81a882cdf4ba9ea3ebeff936f5ecd9865 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_00eb00301df8460cac3ef6487a20cca81a882cdf4ba9ea3ebeff936f5ecd9865->enter($__internal_00eb00301df8460cac3ef6487a20cca81a882cdf4ba9ea3ebeff936f5ecd9865_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_1265b19653046f5fbde8d23d5e6aff9df1f732e19b77254afe80a74a64500c95 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1265b19653046f5fbde8d23d5e6aff9df1f732e19b77254afe80a74a64500c95->enter($__internal_1265b19653046f5fbde8d23d5e6aff9df1f732e19b77254afe80a74a64500c95_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Sexos list</h1>

    <table>
        <thead>
            <tr>
                <th>Idsexo</th>
                <th>Nombresexo</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["sexos"] ?? $this->getContext($context, "sexos")));
        foreach ($context['_seq'] as $context["_key"] => $context["sexo"]) {
            // line 16
            echo "            <tr>
                <td><a href=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("sexo_show", array("idSexo" => $this->getAttribute($context["sexo"], "idSexo", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["sexo"], "idSexo", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["sexo"], "nombreSexo", array()), "html", null, true);
            echo "</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("sexo_show", array("idSexo" => $this->getAttribute($context["sexo"], "idSexo", array()))), "html", null, true);
            echo "\">show</a>
                        </li>
                        <li>
                            <a href=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("sexo_edit", array("idSexo" => $this->getAttribute($context["sexo"], "idSexo", array()))), "html", null, true);
            echo "\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sexo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 36
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("sexo_new");
        echo "\">Create a new sexo</a>
        </li>
    </ul>
";
        
        $__internal_1265b19653046f5fbde8d23d5e6aff9df1f732e19b77254afe80a74a64500c95->leave($__internal_1265b19653046f5fbde8d23d5e6aff9df1f732e19b77254afe80a74a64500c95_prof);

        
        $__internal_00eb00301df8460cac3ef6487a20cca81a882cdf4ba9ea3ebeff936f5ecd9865->leave($__internal_00eb00301df8460cac3ef6487a20cca81a882cdf4ba9ea3ebeff936f5ecd9865_prof);

    }

    public function getTemplateName()
    {
        return ":sexo:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 36,  100 => 31,  88 => 25,  82 => 22,  75 => 18,  69 => 17,  66 => 16,  62 => 15,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Sexos list</h1>

    <table>
        <thead>
            <tr>
                <th>Idsexo</th>
                <th>Nombresexo</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for sexo in sexos %}
            <tr>
                <td><a href=\"{{ path('sexo_show', { 'idSexo': sexo.idSexo }) }}\">{{ sexo.idSexo }}</a></td>
                <td>{{ sexo.nombreSexo }}</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"{{ path('sexo_show', { 'idSexo': sexo.idSexo }) }}\">show</a>
                        </li>
                        <li>
                            <a href=\"{{ path('sexo_edit', { 'idSexo': sexo.idSexo }) }}\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('sexo_new') }}\">Create a new sexo</a>
        </li>
    </ul>
{% endblock %}
", ":sexo:index.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/app/Resources/views/sexo/index.html.twig");
    }
}
