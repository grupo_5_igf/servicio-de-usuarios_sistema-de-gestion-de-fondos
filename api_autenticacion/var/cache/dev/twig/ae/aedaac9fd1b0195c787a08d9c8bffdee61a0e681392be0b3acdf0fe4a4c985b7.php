<?php

/* :estadocivil:new.html.twig */
class __TwigTemplate_9854616f13490f2e781e7fb9fb4b9d74169651def8849eac3b99c40ee6c26215 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":estadocivil:new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3f8ae4f57c82d4033d7270f0c74f0d4c260ede0194ae9a2594c8043f036a1cdb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3f8ae4f57c82d4033d7270f0c74f0d4c260ede0194ae9a2594c8043f036a1cdb->enter($__internal_3f8ae4f57c82d4033d7270f0c74f0d4c260ede0194ae9a2594c8043f036a1cdb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":estadocivil:new.html.twig"));

        $__internal_68a3c060a32e375bffba51e0f678230480c219f15b28d78e0782cd29896cd3a9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_68a3c060a32e375bffba51e0f678230480c219f15b28d78e0782cd29896cd3a9->enter($__internal_68a3c060a32e375bffba51e0f678230480c219f15b28d78e0782cd29896cd3a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":estadocivil:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3f8ae4f57c82d4033d7270f0c74f0d4c260ede0194ae9a2594c8043f036a1cdb->leave($__internal_3f8ae4f57c82d4033d7270f0c74f0d4c260ede0194ae9a2594c8043f036a1cdb_prof);

        
        $__internal_68a3c060a32e375bffba51e0f678230480c219f15b28d78e0782cd29896cd3a9->leave($__internal_68a3c060a32e375bffba51e0f678230480c219f15b28d78e0782cd29896cd3a9_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_5a0ef44ccf5802aea158fef00a9bed99fe1764e3ef1ccfe4305ffd1e04debbf7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5a0ef44ccf5802aea158fef00a9bed99fe1764e3ef1ccfe4305ffd1e04debbf7->enter($__internal_5a0ef44ccf5802aea158fef00a9bed99fe1764e3ef1ccfe4305ffd1e04debbf7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_07d9caa496b5ff7e1c7ca702224431c72c7db9ae505f18a537dbce7f01e60c56 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_07d9caa496b5ff7e1c7ca702224431c72c7db9ae505f18a537dbce7f01e60c56->enter($__internal_07d9caa496b5ff7e1c7ca702224431c72c7db9ae505f18a537dbce7f01e60c56_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Estadocivil creation</h1>

    ";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" />
    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("estadocivil_index");
        echo "\">Back to the list</a>
        </li>
    </ul>
";
        
        $__internal_07d9caa496b5ff7e1c7ca702224431c72c7db9ae505f18a537dbce7f01e60c56->leave($__internal_07d9caa496b5ff7e1c7ca702224431c72c7db9ae505f18a537dbce7f01e60c56_prof);

        
        $__internal_5a0ef44ccf5802aea158fef00a9bed99fe1764e3ef1ccfe4305ffd1e04debbf7->leave($__internal_5a0ef44ccf5802aea158fef00a9bed99fe1764e3ef1ccfe4305ffd1e04debbf7_prof);

    }

    public function getTemplateName()
    {
        return ":estadocivil:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 13,  62 => 9,  57 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Estadocivil creation</h1>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" />
    {{ form_end(form) }}

    <ul>
        <li>
            <a href=\"{{ path('estadocivil_index') }}\">Back to the list</a>
        </li>
    </ul>
{% endblock %}
", ":estadocivil:new.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/app/Resources/views/estadocivil/new.html.twig");
    }
}
