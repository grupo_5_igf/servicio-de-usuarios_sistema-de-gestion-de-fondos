<?php

/* :rol:show.html.twig */
class __TwigTemplate_6cdd8c13ac35a2e2ef4138c933427609d1f38b1545c36c8b845ebc84033b9ef9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":rol:show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e2315604932485785451f6b1225a5e55cd27423d17ea9c09838a8ff0260b67a4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e2315604932485785451f6b1225a5e55cd27423d17ea9c09838a8ff0260b67a4->enter($__internal_e2315604932485785451f6b1225a5e55cd27423d17ea9c09838a8ff0260b67a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":rol:show.html.twig"));

        $__internal_697aaf45d50fee92cd4d353d1f06fad669b3399ec71f181a804b15321b1788f4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_697aaf45d50fee92cd4d353d1f06fad669b3399ec71f181a804b15321b1788f4->enter($__internal_697aaf45d50fee92cd4d353d1f06fad669b3399ec71f181a804b15321b1788f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":rol:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e2315604932485785451f6b1225a5e55cd27423d17ea9c09838a8ff0260b67a4->leave($__internal_e2315604932485785451f6b1225a5e55cd27423d17ea9c09838a8ff0260b67a4_prof);

        
        $__internal_697aaf45d50fee92cd4d353d1f06fad669b3399ec71f181a804b15321b1788f4->leave($__internal_697aaf45d50fee92cd4d353d1f06fad669b3399ec71f181a804b15321b1788f4_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_40ea88bd84d5e5d2293cf6292de26f9e64742a9dadc615b1ad2c643fbf6fdcd6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_40ea88bd84d5e5d2293cf6292de26f9e64742a9dadc615b1ad2c643fbf6fdcd6->enter($__internal_40ea88bd84d5e5d2293cf6292de26f9e64742a9dadc615b1ad2c643fbf6fdcd6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_53bb6da4795eed42ba1f8ba0d369bdad35023a5d5826007f57737cc55ef5a39f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_53bb6da4795eed42ba1f8ba0d369bdad35023a5d5826007f57737cc55ef5a39f->enter($__internal_53bb6da4795eed42ba1f8ba0d369bdad35023a5d5826007f57737cc55ef5a39f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Rol</h1>

    <table>
        <tbody>
            <tr>
                <th>Idrol</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute(($context["rol"] ?? $this->getContext($context, "rol")), "idRol", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Nombrerol</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute(($context["rol"] ?? $this->getContext($context, "rol")), "nombreRol", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 21
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("rol_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("rol_edit", array("idRol" => $this->getAttribute(($context["rol"] ?? $this->getContext($context, "rol")), "idRol", array()))), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 27
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 29
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_53bb6da4795eed42ba1f8ba0d369bdad35023a5d5826007f57737cc55ef5a39f->leave($__internal_53bb6da4795eed42ba1f8ba0d369bdad35023a5d5826007f57737cc55ef5a39f_prof);

        
        $__internal_40ea88bd84d5e5d2293cf6292de26f9e64742a9dadc615b1ad2c643fbf6fdcd6->leave($__internal_40ea88bd84d5e5d2293cf6292de26f9e64742a9dadc615b1ad2c643fbf6fdcd6_prof);

    }

    public function getTemplateName()
    {
        return ":rol:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 29,  86 => 27,  80 => 24,  74 => 21,  64 => 14,  57 => 10,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Rol</h1>

    <table>
        <tbody>
            <tr>
                <th>Idrol</th>
                <td>{{ rol.idRol }}</td>
            </tr>
            <tr>
                <th>Nombrerol</th>
                <td>{{ rol.nombreRol }}</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('rol_index') }}\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('rol_edit', { 'idRol': rol.idRol }) }}\">Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", ":rol:show.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/app/Resources/views/rol/show.html.twig");
    }
}
