<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_e929bec2e6a149b6930b05325c178490343a7a918507da619e91abbae1879c4e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ab241558a9f0087cbbafb5aea31dfe5f4403d2cca2d86f46952ef9a6f6d19f28 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ab241558a9f0087cbbafb5aea31dfe5f4403d2cca2d86f46952ef9a6f6d19f28->enter($__internal_ab241558a9f0087cbbafb5aea31dfe5f4403d2cca2d86f46952ef9a6f6d19f28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        $__internal_2655d77b7d09e707de1135549970d6d98fb76e80b004be2e02897cfd4df9ad6a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2655d77b7d09e707de1135549970d6d98fb76e80b004be2e02897cfd4df9ad6a->enter($__internal_2655d77b7d09e707de1135549970d6d98fb76e80b004be2e02897cfd4df9ad6a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_ab241558a9f0087cbbafb5aea31dfe5f4403d2cca2d86f46952ef9a6f6d19f28->leave($__internal_ab241558a9f0087cbbafb5aea31dfe5f4403d2cca2d86f46952ef9a6f6d19f28_prof);

        
        $__internal_2655d77b7d09e707de1135549970d6d98fb76e80b004be2e02897cfd4df9ad6a->leave($__internal_2655d77b7d09e707de1135549970d6d98fb76e80b004be2e02897cfd4df9ad6a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
", "@Framework/Form/submit_widget.html.php", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/submit_widget.html.php");
    }
}
