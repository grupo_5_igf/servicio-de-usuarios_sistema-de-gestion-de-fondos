<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_bd261761b9df5032b9aa3cb8b0af94d0d155b6dd5a02f66fcea2fc15316666b1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9111a7b4dea11201f7edf789df4b6e101e9ed5018298d6fbb28fc8cb9f60d4d8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9111a7b4dea11201f7edf789df4b6e101e9ed5018298d6fbb28fc8cb9f60d4d8->enter($__internal_9111a7b4dea11201f7edf789df4b6e101e9ed5018298d6fbb28fc8cb9f60d4d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        $__internal_8af26237412b1448768358fa9f63a25239bbb7a2cba473881da2879332ac7c9a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8af26237412b1448768358fa9f63a25239bbb7a2cba473881da2879332ac7c9a->enter($__internal_8af26237412b1448768358fa9f63a25239bbb7a2cba473881da2879332ac7c9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_9111a7b4dea11201f7edf789df4b6e101e9ed5018298d6fbb28fc8cb9f60d4d8->leave($__internal_9111a7b4dea11201f7edf789df4b6e101e9ed5018298d6fbb28fc8cb9f60d4d8_prof);

        
        $__internal_8af26237412b1448768358fa9f63a25239bbb7a2cba473881da2879332ac7c9a->leave($__internal_8af26237412b1448768358fa9f63a25239bbb7a2cba473881da2879332ac7c9a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
", "@Framework/Form/url_widget.html.php", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/url_widget.html.php");
    }
}
