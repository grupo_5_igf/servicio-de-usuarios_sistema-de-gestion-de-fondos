<?php

/* WebProfilerBundle:Collector:exception.html.twig */
class __TwigTemplate_cf162f25fa3b6f82cee69ba6f883fb7c82621dcf246f7c18429b115a3511835f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7f6c8ac221982ff2d6c7d64771eb80de01969891bb9cdde0a1446445f2cc76f4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7f6c8ac221982ff2d6c7d64771eb80de01969891bb9cdde0a1446445f2cc76f4->enter($__internal_7f6c8ac221982ff2d6c7d64771eb80de01969891bb9cdde0a1446445f2cc76f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.html.twig"));

        $__internal_ba14767b2310ab57e99ff5b55d3cc834f3a3313382aebce40072acc6bab2d1aa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ba14767b2310ab57e99ff5b55d3cc834f3a3313382aebce40072acc6bab2d1aa->enter($__internal_ba14767b2310ab57e99ff5b55d3cc834f3a3313382aebce40072acc6bab2d1aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7f6c8ac221982ff2d6c7d64771eb80de01969891bb9cdde0a1446445f2cc76f4->leave($__internal_7f6c8ac221982ff2d6c7d64771eb80de01969891bb9cdde0a1446445f2cc76f4_prof);

        
        $__internal_ba14767b2310ab57e99ff5b55d3cc834f3a3313382aebce40072acc6bab2d1aa->leave($__internal_ba14767b2310ab57e99ff5b55d3cc834f3a3313382aebce40072acc6bab2d1aa_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_76b85468303b3a58b62f73ca1e7c62bf9bfff53351bef886fdf490d625104458 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_76b85468303b3a58b62f73ca1e7c62bf9bfff53351bef886fdf490d625104458->enter($__internal_76b85468303b3a58b62f73ca1e7c62bf9bfff53351bef886fdf490d625104458_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_29b64d7af8141f8035028d84079910862a42979136e2f919cc5257b5c2069d9d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_29b64d7af8141f8035028d84079910862a42979136e2f919cc5257b5c2069d9d->enter($__internal_29b64d7af8141f8035028d84079910862a42979136e2f919cc5257b5c2069d9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_29b64d7af8141f8035028d84079910862a42979136e2f919cc5257b5c2069d9d->leave($__internal_29b64d7af8141f8035028d84079910862a42979136e2f919cc5257b5c2069d9d_prof);

        
        $__internal_76b85468303b3a58b62f73ca1e7c62bf9bfff53351bef886fdf490d625104458->leave($__internal_76b85468303b3a58b62f73ca1e7c62bf9bfff53351bef886fdf490d625104458_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_95cdddbf950532fa974e23f640812a36c25fbe10d6731772a8fbadf2dbf75302 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_95cdddbf950532fa974e23f640812a36c25fbe10d6731772a8fbadf2dbf75302->enter($__internal_95cdddbf950532fa974e23f640812a36c25fbe10d6731772a8fbadf2dbf75302_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_b5e56939198b2cb645e9921a0e6f0915eb4cb52d9bc4b01c3cbb8abe4ec27b6a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b5e56939198b2cb645e9921a0e6f0915eb4cb52d9bc4b01c3cbb8abe4ec27b6a->enter($__internal_b5e56939198b2cb645e9921a0e6f0915eb4cb52d9bc4b01c3cbb8abe4ec27b6a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_b5e56939198b2cb645e9921a0e6f0915eb4cb52d9bc4b01c3cbb8abe4ec27b6a->leave($__internal_b5e56939198b2cb645e9921a0e6f0915eb4cb52d9bc4b01c3cbb8abe4ec27b6a_prof);

        
        $__internal_95cdddbf950532fa974e23f640812a36c25fbe10d6731772a8fbadf2dbf75302->leave($__internal_95cdddbf950532fa974e23f640812a36c25fbe10d6731772a8fbadf2dbf75302_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_65f67d79344570d6174bc7201edc6038425d688fac3fb25ba86f37d73d89af5c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_65f67d79344570d6174bc7201edc6038425d688fac3fb25ba86f37d73d89af5c->enter($__internal_65f67d79344570d6174bc7201edc6038425d688fac3fb25ba86f37d73d89af5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_68be689e427701730b5d8ef2aaae3b523ef1b5d9bf7af8cdcd0cbe744521dc48 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_68be689e427701730b5d8ef2aaae3b523ef1b5d9bf7af8cdcd0cbe744521dc48->enter($__internal_68be689e427701730b5d8ef2aaae3b523ef1b5d9bf7af8cdcd0cbe744521dc48_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_68be689e427701730b5d8ef2aaae3b523ef1b5d9bf7af8cdcd0cbe744521dc48->leave($__internal_68be689e427701730b5d8ef2aaae3b523ef1b5d9bf7af8cdcd0cbe744521dc48_prof);

        
        $__internal_65f67d79344570d6174bc7201edc6038425d688fac3fb25ba86f37d73d89af5c->leave($__internal_65f67d79344570d6174bc7201edc6038425d688fac3fb25ba86f37d73d89af5c_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "WebProfilerBundle:Collector:exception.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
