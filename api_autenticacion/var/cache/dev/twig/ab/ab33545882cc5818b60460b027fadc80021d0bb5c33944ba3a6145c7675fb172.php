<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_589da9bf4712d58ce229ce2f01c52a7859f00baa0fb140d7796a05f5af2b0683 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_60ee2bd56577cc8d6a799e4dacb9be295f81756eb72179e5b9f73f5c77892913 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_60ee2bd56577cc8d6a799e4dacb9be295f81756eb72179e5b9f73f5c77892913->enter($__internal_60ee2bd56577cc8d6a799e4dacb9be295f81756eb72179e5b9f73f5c77892913_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        $__internal_9597213b43422fa732d92c4601a2cee2bd86a162df90152e4062baf589d4488d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9597213b43422fa732d92c4601a2cee2bd86a162df90152e4062baf589d4488d->enter($__internal_9597213b43422fa732d92c4601a2cee2bd86a162df90152e4062baf589d4488d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_60ee2bd56577cc8d6a799e4dacb9be295f81756eb72179e5b9f73f5c77892913->leave($__internal_60ee2bd56577cc8d6a799e4dacb9be295f81756eb72179e5b9f73f5c77892913_prof);

        
        $__internal_9597213b43422fa732d92c4601a2cee2bd86a162df90152e4062baf589d4488d->leave($__internal_9597213b43422fa732d92c4601a2cee2bd86a162df90152e4062baf589d4488d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
", "@Framework/Form/percent_widget.html.php", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/percent_widget.html.php");
    }
}
