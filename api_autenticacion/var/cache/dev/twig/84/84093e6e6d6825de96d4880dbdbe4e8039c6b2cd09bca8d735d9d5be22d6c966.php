<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_b790b556d58827420fdcc2bef1b5d2eca9c8fb56c50a3e391e0f0475be3dd7f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1e8adf9a3dc9d1b0ea85fbec1af2f810284c4ffd1b00c7c28f0b41d4cf291939 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1e8adf9a3dc9d1b0ea85fbec1af2f810284c4ffd1b00c7c28f0b41d4cf291939->enter($__internal_1e8adf9a3dc9d1b0ea85fbec1af2f810284c4ffd1b00c7c28f0b41d4cf291939_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        $__internal_f447e3b913c3d3742773cce70e445d03f9831327ac4f17fd8517ddc5ddd31278 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f447e3b913c3d3742773cce70e445d03f9831327ac4f17fd8517ddc5ddd31278->enter($__internal_f447e3b913c3d3742773cce70e445d03f9831327ac4f17fd8517ddc5ddd31278_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_1e8adf9a3dc9d1b0ea85fbec1af2f810284c4ffd1b00c7c28f0b41d4cf291939->leave($__internal_1e8adf9a3dc9d1b0ea85fbec1af2f810284c4ffd1b00c7c28f0b41d4cf291939_prof);

        
        $__internal_f447e3b913c3d3742773cce70e445d03f9831327ac4f17fd8517ddc5ddd31278->leave($__internal_f447e3b913c3d3742773cce70e445d03f9831327ac4f17fd8517ddc5ddd31278_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
", "@Framework/Form/textarea_widget.html.php", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/textarea_widget.html.php");
    }
}
