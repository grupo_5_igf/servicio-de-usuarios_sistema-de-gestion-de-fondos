<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_3af4a99c1cb655a4825f0a6e01551d674b52d52b622e47d73e3845cb1496152a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_120d6c52feb3ea95f596ce1343d7900b3e689898af8b7e3e96c3e29a7d6297f9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_120d6c52feb3ea95f596ce1343d7900b3e689898af8b7e3e96c3e29a7d6297f9->enter($__internal_120d6c52feb3ea95f596ce1343d7900b3e689898af8b7e3e96c3e29a7d6297f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $__internal_5605a0846315fa2efcdcd005767a21e1ac90db62fed9d881368772f673337e10 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5605a0846315fa2efcdcd005767a21e1ac90db62fed9d881368772f673337e10->enter($__internal_5605a0846315fa2efcdcd005767a21e1ac90db62fed9d881368772f673337e10_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_120d6c52feb3ea95f596ce1343d7900b3e689898af8b7e3e96c3e29a7d6297f9->leave($__internal_120d6c52feb3ea95f596ce1343d7900b3e689898af8b7e3e96c3e29a7d6297f9_prof);

        
        $__internal_5605a0846315fa2efcdcd005767a21e1ac90db62fed9d881368772f673337e10->leave($__internal_5605a0846315fa2efcdcd005767a21e1ac90db62fed9d881368772f673337e10_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_335e2a57a33976a4fcbc748355448c0e25dd3e56e2ffe9b0b1195ce68019ab8b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_335e2a57a33976a4fcbc748355448c0e25dd3e56e2ffe9b0b1195ce68019ab8b->enter($__internal_335e2a57a33976a4fcbc748355448c0e25dd3e56e2ffe9b0b1195ce68019ab8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_b6363ba70dc1f3b46e2c33a3a38215530dfe9517d94f299383dd15842fca685e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b6363ba70dc1f3b46e2c33a3a38215530dfe9517d94f299383dd15842fca685e->enter($__internal_b6363ba70dc1f3b46e2c33a3a38215530dfe9517d94f299383dd15842fca685e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_b6363ba70dc1f3b46e2c33a3a38215530dfe9517d94f299383dd15842fca685e->leave($__internal_b6363ba70dc1f3b46e2c33a3a38215530dfe9517d94f299383dd15842fca685e_prof);

        
        $__internal_335e2a57a33976a4fcbc748355448c0e25dd3e56e2ffe9b0b1195ce68019ab8b->leave($__internal_335e2a57a33976a4fcbc748355448c0e25dd3e56e2ffe9b0b1195ce68019ab8b_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_970c83e25cdaf93435c333871fa8ec2dca7bec7cdb6fdc607dbd29485e8b7289 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_970c83e25cdaf93435c333871fa8ec2dca7bec7cdb6fdc607dbd29485e8b7289->enter($__internal_970c83e25cdaf93435c333871fa8ec2dca7bec7cdb6fdc607dbd29485e8b7289_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_d8a6c1856b011562917be8d7aef14f8078d790cf37455d93a80adbf5fecea05f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d8a6c1856b011562917be8d7aef14f8078d790cf37455d93a80adbf5fecea05f->enter($__internal_d8a6c1856b011562917be8d7aef14f8078d790cf37455d93a80adbf5fecea05f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_d8a6c1856b011562917be8d7aef14f8078d790cf37455d93a80adbf5fecea05f->leave($__internal_d8a6c1856b011562917be8d7aef14f8078d790cf37455d93a80adbf5fecea05f_prof);

        
        $__internal_970c83e25cdaf93435c333871fa8ec2dca7bec7cdb6fdc607dbd29485e8b7289->leave($__internal_970c83e25cdaf93435c333871fa8ec2dca7bec7cdb6fdc607dbd29485e8b7289_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_469b14ffa23bcd6efcea3cbf6315922f3124092ad68a02859e6e74643ce92da1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_469b14ffa23bcd6efcea3cbf6315922f3124092ad68a02859e6e74643ce92da1->enter($__internal_469b14ffa23bcd6efcea3cbf6315922f3124092ad68a02859e6e74643ce92da1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_bb60b7ae1e836aaee8c98a4c0c63650ed8cb9d29b80d3ae7b6abedf9f785f138 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bb60b7ae1e836aaee8c98a4c0c63650ed8cb9d29b80d3ae7b6abedf9f785f138->enter($__internal_bb60b7ae1e836aaee8c98a4c0c63650ed8cb9d29b80d3ae7b6abedf9f785f138_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_bb60b7ae1e836aaee8c98a4c0c63650ed8cb9d29b80d3ae7b6abedf9f785f138->leave($__internal_bb60b7ae1e836aaee8c98a4c0c63650ed8cb9d29b80d3ae7b6abedf9f785f138_prof);

        
        $__internal_469b14ffa23bcd6efcea3cbf6315922f3124092ad68a02859e6e74643ce92da1->leave($__internal_469b14ffa23bcd6efcea3cbf6315922f3124092ad68a02859e6e74643ce92da1_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "WebProfilerBundle:Collector:router.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
