<?php

/* :usuario:show.html.twig */
class __TwigTemplate_8c5e5fb5d322345efe838f15aa3e83debf79e792f518506d147c796af50e1783 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":usuario:show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cf3359c66b1a85d76b5eb67c037c4354bd8bdf9b7e37065296b2e163dc5f55a0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cf3359c66b1a85d76b5eb67c037c4354bd8bdf9b7e37065296b2e163dc5f55a0->enter($__internal_cf3359c66b1a85d76b5eb67c037c4354bd8bdf9b7e37065296b2e163dc5f55a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":usuario:show.html.twig"));

        $__internal_66573b57f10fc06bb8851d25613c6986f2bfefdcd18ac1b8b4413df1ac59ea89 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_66573b57f10fc06bb8851d25613c6986f2bfefdcd18ac1b8b4413df1ac59ea89->enter($__internal_66573b57f10fc06bb8851d25613c6986f2bfefdcd18ac1b8b4413df1ac59ea89_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":usuario:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cf3359c66b1a85d76b5eb67c037c4354bd8bdf9b7e37065296b2e163dc5f55a0->leave($__internal_cf3359c66b1a85d76b5eb67c037c4354bd8bdf9b7e37065296b2e163dc5f55a0_prof);

        
        $__internal_66573b57f10fc06bb8851d25613c6986f2bfefdcd18ac1b8b4413df1ac59ea89->leave($__internal_66573b57f10fc06bb8851d25613c6986f2bfefdcd18ac1b8b4413df1ac59ea89_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_5e6497cdf020e35e24bfeea8d14dcb5278c4c1f38a5891af3a2115329c962e12 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5e6497cdf020e35e24bfeea8d14dcb5278c4c1f38a5891af3a2115329c962e12->enter($__internal_5e6497cdf020e35e24bfeea8d14dcb5278c4c1f38a5891af3a2115329c962e12_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_254b23930e3722abe741035cb61441178eaf1586ed01414498daf625508676f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_254b23930e3722abe741035cb61441178eaf1586ed01414498daf625508676f0->enter($__internal_254b23930e3722abe741035cb61441178eaf1586ed01414498daf625508676f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Usuario</h1>

    <table>
        <tbody>
            <tr>
                <th>Idusuario</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute(($context["usuario"] ?? $this->getContext($context, "usuario")), "idUsuario", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Correo</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute(($context["usuario"] ?? $this->getContext($context, "usuario")), "correo", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Contrasenia</th>
                <td>";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute(($context["usuario"] ?? $this->getContext($context, "usuario")), "contrasenia", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Estado</th>
                <td>";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute(($context["usuario"] ?? $this->getContext($context, "usuario")), "estado", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 29
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usuario_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usuario_edit", array("idUsuario" => $this->getAttribute(($context["usuario"] ?? $this->getContext($context, "usuario")), "idUsuario", array()))), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 35
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 37
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_254b23930e3722abe741035cb61441178eaf1586ed01414498daf625508676f0->leave($__internal_254b23930e3722abe741035cb61441178eaf1586ed01414498daf625508676f0_prof);

        
        $__internal_5e6497cdf020e35e24bfeea8d14dcb5278c4c1f38a5891af3a2115329c962e12->leave($__internal_5e6497cdf020e35e24bfeea8d14dcb5278c4c1f38a5891af3a2115329c962e12_prof);

    }

    public function getTemplateName()
    {
        return ":usuario:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 37,  100 => 35,  94 => 32,  88 => 29,  78 => 22,  71 => 18,  64 => 14,  57 => 10,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Usuario</h1>

    <table>
        <tbody>
            <tr>
                <th>Idusuario</th>
                <td>{{ usuario.idUsuario }}</td>
            </tr>
            <tr>
                <th>Correo</th>
                <td>{{ usuario.correo }}</td>
            </tr>
            <tr>
                <th>Contrasenia</th>
                <td>{{ usuario.contrasenia }}</td>
            </tr>
            <tr>
                <th>Estado</th>
                <td>{{ usuario.estado }}</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('usuario_index') }}\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('usuario_edit', { 'idUsuario': usuario.idUsuario }) }}\">Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", ":usuario:show.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/app/Resources/views/usuario/show.html.twig");
    }
}
