<?php

/* :persona:show.html.twig */
class __TwigTemplate_9129e6c246f547cfae761487463f6682fe77ed16050fc9d8a585511d5d5c94b6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":persona:show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_21f8f0238103583f84544f3befe743c085c8f3740b810b250ca936fb0023c481 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_21f8f0238103583f84544f3befe743c085c8f3740b810b250ca936fb0023c481->enter($__internal_21f8f0238103583f84544f3befe743c085c8f3740b810b250ca936fb0023c481_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":persona:show.html.twig"));

        $__internal_50fce576ffba28478c1b9991701730669e6832cf217cee8c1c71d593a44c8706 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_50fce576ffba28478c1b9991701730669e6832cf217cee8c1c71d593a44c8706->enter($__internal_50fce576ffba28478c1b9991701730669e6832cf217cee8c1c71d593a44c8706_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":persona:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_21f8f0238103583f84544f3befe743c085c8f3740b810b250ca936fb0023c481->leave($__internal_21f8f0238103583f84544f3befe743c085c8f3740b810b250ca936fb0023c481_prof);

        
        $__internal_50fce576ffba28478c1b9991701730669e6832cf217cee8c1c71d593a44c8706->leave($__internal_50fce576ffba28478c1b9991701730669e6832cf217cee8c1c71d593a44c8706_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_a312ba833281d28bad28ad4932652e20ab74fb8485f91083a4f9dca613c374de = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a312ba833281d28bad28ad4932652e20ab74fb8485f91083a4f9dca613c374de->enter($__internal_a312ba833281d28bad28ad4932652e20ab74fb8485f91083a4f9dca613c374de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_08bc1a01b71a6c5315c516a7cdd184d7c5186ba425fe7afa07b699ba76d7b7cd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_08bc1a01b71a6c5315c516a7cdd184d7c5186ba425fe7afa07b699ba76d7b7cd->enter($__internal_08bc1a01b71a6c5315c516a7cdd184d7c5186ba425fe7afa07b699ba76d7b7cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Persona</h1>

    <table>
        <tbody>
            <tr>
                <th>Idpersona</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute(($context["persona"] ?? $this->getContext($context, "persona")), "idPersona", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Nombre</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute(($context["persona"] ?? $this->getContext($context, "persona")), "nombre", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Apellido</th>
                <td>";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute(($context["persona"] ?? $this->getContext($context, "persona")), "apellido", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Dui</th>
                <td>";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute(($context["persona"] ?? $this->getContext($context, "persona")), "dui", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Nit</th>
                <td>";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute(($context["persona"] ?? $this->getContext($context, "persona")), "nit", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Direccion</th>
                <td>";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute(($context["persona"] ?? $this->getContext($context, "persona")), "direccion", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Telefono</th>
                <td>";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute(($context["persona"] ?? $this->getContext($context, "persona")), "telefono", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 41
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("persona_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("persona_edit", array("idPersona" => $this->getAttribute(($context["persona"] ?? $this->getContext($context, "persona")), "idPersona", array()))), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 47
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 49
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_08bc1a01b71a6c5315c516a7cdd184d7c5186ba425fe7afa07b699ba76d7b7cd->leave($__internal_08bc1a01b71a6c5315c516a7cdd184d7c5186ba425fe7afa07b699ba76d7b7cd_prof);

        
        $__internal_a312ba833281d28bad28ad4932652e20ab74fb8485f91083a4f9dca613c374de->leave($__internal_a312ba833281d28bad28ad4932652e20ab74fb8485f91083a4f9dca613c374de_prof);

    }

    public function getTemplateName()
    {
        return ":persona:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 49,  121 => 47,  115 => 44,  109 => 41,  99 => 34,  92 => 30,  85 => 26,  78 => 22,  71 => 18,  64 => 14,  57 => 10,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Persona</h1>

    <table>
        <tbody>
            <tr>
                <th>Idpersona</th>
                <td>{{ persona.idPersona }}</td>
            </tr>
            <tr>
                <th>Nombre</th>
                <td>{{ persona.nombre }}</td>
            </tr>
            <tr>
                <th>Apellido</th>
                <td>{{ persona.apellido }}</td>
            </tr>
            <tr>
                <th>Dui</th>
                <td>{{ persona.dui }}</td>
            </tr>
            <tr>
                <th>Nit</th>
                <td>{{ persona.nit }}</td>
            </tr>
            <tr>
                <th>Direccion</th>
                <td>{{ persona.direccion }}</td>
            </tr>
            <tr>
                <th>Telefono</th>
                <td>{{ persona.telefono }}</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('persona_index') }}\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('persona_edit', { 'idPersona': persona.idPersona }) }}\">Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", ":persona:show.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/app/Resources/views/persona/show.html.twig");
    }
}
