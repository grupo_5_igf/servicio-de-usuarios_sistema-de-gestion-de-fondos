<?php

/* TwigBundle:Exception:error.rdf.twig */
class __TwigTemplate_9c85e1c6dc6cd7fd4d29b0657daf3d563e349ecb6f696cf03a9b48e4804332a5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8d72fc52c67512b53073b1c3701e73616389f2958559e09d435881c2291b60de = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8d72fc52c67512b53073b1c3701e73616389f2958559e09d435881c2291b60de->enter($__internal_8d72fc52c67512b53073b1c3701e73616389f2958559e09d435881c2291b60de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        $__internal_d7b43b7ecaabac52de2bce0bc8dbf3da1cd678994254e27960ced427929e975e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d7b43b7ecaabac52de2bce0bc8dbf3da1cd678994254e27960ced427929e975e->enter($__internal_d7b43b7ecaabac52de2bce0bc8dbf3da1cd678994254e27960ced427929e975e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/error.xml.twig");
        echo "
";
        
        $__internal_8d72fc52c67512b53073b1c3701e73616389f2958559e09d435881c2291b60de->leave($__internal_8d72fc52c67512b53073b1c3701e73616389f2958559e09d435881c2291b60de_prof);

        
        $__internal_d7b43b7ecaabac52de2bce0bc8dbf3da1cd678994254e27960ced427929e975e->leave($__internal_d7b43b7ecaabac52de2bce0bc8dbf3da1cd678994254e27960ced427929e975e_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/error.xml.twig') }}
", "TwigBundle:Exception:error.rdf.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.rdf.twig");
    }
}
