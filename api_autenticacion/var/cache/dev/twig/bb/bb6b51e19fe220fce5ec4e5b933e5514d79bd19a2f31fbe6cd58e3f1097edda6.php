<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_90c515f661ba0957029f62e7816d6064d95afb900007341e45d52ce01e9b60ac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_eba43b9086612de527458fec690c03806a32312c80121b3bbc3c7e38a6c3c07d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eba43b9086612de527458fec690c03806a32312c80121b3bbc3c7e38a6c3c07d->enter($__internal_eba43b9086612de527458fec690c03806a32312c80121b3bbc3c7e38a6c3c07d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        $__internal_5353644ad71d8c1e4210c90c3e1121bcb491a22e5e44e2235f104f106bb849d7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5353644ad71d8c1e4210c90c3e1121bcb491a22e5e44e2235f104f106bb849d7->enter($__internal_5353644ad71d8c1e4210c90c3e1121bcb491a22e5e44e2235f104f106bb849d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
";
        
        $__internal_eba43b9086612de527458fec690c03806a32312c80121b3bbc3c7e38a6c3c07d->leave($__internal_eba43b9086612de527458fec690c03806a32312c80121b3bbc3c7e38a6c3c07d_prof);

        
        $__internal_5353644ad71d8c1e4210c90c3e1121bcb491a22e5e44e2235f104f106bb849d7->leave($__internal_5353644ad71d8c1e4210c90c3e1121bcb491a22e5e44e2235f104f106bb849d7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
", "@Framework/FormTable/hidden_row.html.php", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/hidden_row.html.php");
    }
}
