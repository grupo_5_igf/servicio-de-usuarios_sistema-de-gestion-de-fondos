<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_693ce7bb96d9146ba67ab268f7a81cdf4f1608e56b661641748f705c7b7a2e1b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_df0eb33dd9827cf5a3fc571068a812bc371a7614b628dd93e6f9dea6b3d8eb45 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_df0eb33dd9827cf5a3fc571068a812bc371a7614b628dd93e6f9dea6b3d8eb45->enter($__internal_df0eb33dd9827cf5a3fc571068a812bc371a7614b628dd93e6f9dea6b3d8eb45_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        $__internal_20b5d8b86743099f2bc904033482fa6a77a3296877527c4c9951577cb6b96b9f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_20b5d8b86743099f2bc904033482fa6a77a3296877527c4c9951577cb6b96b9f->enter($__internal_20b5d8b86743099f2bc904033482fa6a77a3296877527c4c9951577cb6b96b9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_df0eb33dd9827cf5a3fc571068a812bc371a7614b628dd93e6f9dea6b3d8eb45->leave($__internal_df0eb33dd9827cf5a3fc571068a812bc371a7614b628dd93e6f9dea6b3d8eb45_prof);

        
        $__internal_20b5d8b86743099f2bc904033482fa6a77a3296877527c4c9951577cb6b96b9f->leave($__internal_20b5d8b86743099f2bc904033482fa6a77a3296877527c4c9951577cb6b96b9f_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_47488589189ae37c0acde2c72bcb86e7b048b5f32bd0619ebdf77d196dca18d1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_47488589189ae37c0acde2c72bcb86e7b048b5f32bd0619ebdf77d196dca18d1->enter($__internal_47488589189ae37c0acde2c72bcb86e7b048b5f32bd0619ebdf77d196dca18d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_c969e8df0e1830c05b0287587bd82a1e88e224155aaf5d50bd4e77866199b459 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c969e8df0e1830c05b0287587bd82a1e88e224155aaf5d50bd4e77866199b459->enter($__internal_c969e8df0e1830c05b0287587bd82a1e88e224155aaf5d50bd4e77866199b459_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_c969e8df0e1830c05b0287587bd82a1e88e224155aaf5d50bd4e77866199b459->leave($__internal_c969e8df0e1830c05b0287587bd82a1e88e224155aaf5d50bd4e77866199b459_prof);

        
        $__internal_47488589189ae37c0acde2c72bcb86e7b048b5f32bd0619ebdf77d196dca18d1->leave($__internal_47488589189ae37c0acde2c72bcb86e7b048b5f32bd0619ebdf77d196dca18d1_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "WebProfilerBundle:Profiler:ajax_layout.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}
