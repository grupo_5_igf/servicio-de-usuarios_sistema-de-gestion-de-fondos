<?php

/* :usuario:new.html.twig */
class __TwigTemplate_adb14c3b72506161c5efb3f22401d4bb69093b25b7d4a04d08ff965991c89971 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":usuario:new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4bf3c5dd670665ccf383b70d83694b6fb29f1e1544959b2e1c97d82b5d716c02 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4bf3c5dd670665ccf383b70d83694b6fb29f1e1544959b2e1c97d82b5d716c02->enter($__internal_4bf3c5dd670665ccf383b70d83694b6fb29f1e1544959b2e1c97d82b5d716c02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":usuario:new.html.twig"));

        $__internal_7502843ef676518d87686edc129dff79ddc11378589d50b00d2e0f2dff51c6f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7502843ef676518d87686edc129dff79ddc11378589d50b00d2e0f2dff51c6f0->enter($__internal_7502843ef676518d87686edc129dff79ddc11378589d50b00d2e0f2dff51c6f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":usuario:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4bf3c5dd670665ccf383b70d83694b6fb29f1e1544959b2e1c97d82b5d716c02->leave($__internal_4bf3c5dd670665ccf383b70d83694b6fb29f1e1544959b2e1c97d82b5d716c02_prof);

        
        $__internal_7502843ef676518d87686edc129dff79ddc11378589d50b00d2e0f2dff51c6f0->leave($__internal_7502843ef676518d87686edc129dff79ddc11378589d50b00d2e0f2dff51c6f0_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_4c9600e277e0ee7cb59ab7b1ed2bf9f8dfdf7c57ae3c3eaaadda7f54453b1ec7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4c9600e277e0ee7cb59ab7b1ed2bf9f8dfdf7c57ae3c3eaaadda7f54453b1ec7->enter($__internal_4c9600e277e0ee7cb59ab7b1ed2bf9f8dfdf7c57ae3c3eaaadda7f54453b1ec7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_01065b6d8a518c73f180211e4c8d2dbae8f715e54f7d2f3561d52e0e06b60431 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_01065b6d8a518c73f180211e4c8d2dbae8f715e54f7d2f3561d52e0e06b60431->enter($__internal_01065b6d8a518c73f180211e4c8d2dbae8f715e54f7d2f3561d52e0e06b60431_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Usuario creation</h1>

    ";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" />
    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usuario_index");
        echo "\">Back to the list</a>
        </li>
    </ul>
";
        
        $__internal_01065b6d8a518c73f180211e4c8d2dbae8f715e54f7d2f3561d52e0e06b60431->leave($__internal_01065b6d8a518c73f180211e4c8d2dbae8f715e54f7d2f3561d52e0e06b60431_prof);

        
        $__internal_4c9600e277e0ee7cb59ab7b1ed2bf9f8dfdf7c57ae3c3eaaadda7f54453b1ec7->leave($__internal_4c9600e277e0ee7cb59ab7b1ed2bf9f8dfdf7c57ae3c3eaaadda7f54453b1ec7_prof);

    }

    public function getTemplateName()
    {
        return ":usuario:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 13,  62 => 9,  57 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Usuario creation</h1>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" />
    {{ form_end(form) }}

    <ul>
        <li>
            <a href=\"{{ path('usuario_index') }}\">Back to the list</a>
        </li>
    </ul>
{% endblock %}
", ":usuario:new.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/app/Resources/views/usuario/new.html.twig");
    }
}
