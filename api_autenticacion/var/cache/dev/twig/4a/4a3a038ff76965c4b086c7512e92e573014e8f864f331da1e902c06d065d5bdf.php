<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_b02dfd81140eb2a24a8fb18cd6332cff44805b1d92cb2302754604add0b319f9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cec1428aac13fb545b531e061e9ccc1923b49e21f2f168e2595db65071530683 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cec1428aac13fb545b531e061e9ccc1923b49e21f2f168e2595db65071530683->enter($__internal_cec1428aac13fb545b531e061e9ccc1923b49e21f2f168e2595db65071530683_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        $__internal_0eb5fef600969b450def589afa304e18c8f5c9624a00983779faad02e9d9473c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0eb5fef600969b450def589afa304e18c8f5c9624a00983779faad02e9d9473c->enter($__internal_0eb5fef600969b450def589afa304e18c8f5c9624a00983779faad02e9d9473c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_cec1428aac13fb545b531e061e9ccc1923b49e21f2f168e2595db65071530683->leave($__internal_cec1428aac13fb545b531e061e9ccc1923b49e21f2f168e2595db65071530683_prof);

        
        $__internal_0eb5fef600969b450def589afa304e18c8f5c9624a00983779faad02e9d9473c->leave($__internal_0eb5fef600969b450def589afa304e18c8f5c9624a00983779faad02e9d9473c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
", "@Framework/Form/form_enctype.html.php", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_enctype.html.php");
    }
}
