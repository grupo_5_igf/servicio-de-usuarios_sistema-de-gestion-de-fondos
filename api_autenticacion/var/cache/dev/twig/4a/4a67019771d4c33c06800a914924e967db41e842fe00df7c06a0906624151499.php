<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_1878275fdbc32b660cfbee6afea5d6e15c55adb100b0dbddcb4be415750c22a8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2a690d024df926ae938c44d5033477ae8b07b717b003356208ad0d4c95188ed5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2a690d024df926ae938c44d5033477ae8b07b717b003356208ad0d4c95188ed5->enter($__internal_2a690d024df926ae938c44d5033477ae8b07b717b003356208ad0d4c95188ed5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $__internal_f168795020b909e6373ac48b7fea282f5a9bb8ce5418ac9d15f74b3343977ebd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f168795020b909e6373ac48b7fea282f5a9bb8ce5418ac9d15f74b3343977ebd->enter($__internal_f168795020b909e6373ac48b7fea282f5a9bb8ce5418ac9d15f74b3343977ebd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2a690d024df926ae938c44d5033477ae8b07b717b003356208ad0d4c95188ed5->leave($__internal_2a690d024df926ae938c44d5033477ae8b07b717b003356208ad0d4c95188ed5_prof);

        
        $__internal_f168795020b909e6373ac48b7fea282f5a9bb8ce5418ac9d15f74b3343977ebd->leave($__internal_f168795020b909e6373ac48b7fea282f5a9bb8ce5418ac9d15f74b3343977ebd_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_74ef686f7847cf2df76356bfad96910642d7c3b3db26bcb24c1c16ad02137fb9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_74ef686f7847cf2df76356bfad96910642d7c3b3db26bcb24c1c16ad02137fb9->enter($__internal_74ef686f7847cf2df76356bfad96910642d7c3b3db26bcb24c1c16ad02137fb9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_b72703571106d77d3addd4dd6d829ffc476fe2fc04cf31fcf8e7766cb16657c0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b72703571106d77d3addd4dd6d829ffc476fe2fc04cf31fcf8e7766cb16657c0->enter($__internal_b72703571106d77d3addd4dd6d829ffc476fe2fc04cf31fcf8e7766cb16657c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_b72703571106d77d3addd4dd6d829ffc476fe2fc04cf31fcf8e7766cb16657c0->leave($__internal_b72703571106d77d3addd4dd6d829ffc476fe2fc04cf31fcf8e7766cb16657c0_prof);

        
        $__internal_74ef686f7847cf2df76356bfad96910642d7c3b3db26bcb24c1c16ad02137fb9->leave($__internal_74ef686f7847cf2df76356bfad96910642d7c3b3db26bcb24c1c16ad02137fb9_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_dc6aa51b4dd1aa8e16327b44fd51cab564718dc5550d9dc055a674e4020167bb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dc6aa51b4dd1aa8e16327b44fd51cab564718dc5550d9dc055a674e4020167bb->enter($__internal_dc6aa51b4dd1aa8e16327b44fd51cab564718dc5550d9dc055a674e4020167bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_c1dd7c458783cc78429b5b3378d8078450b1639c9962efddffa37227473eb8b3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c1dd7c458783cc78429b5b3378d8078450b1639c9962efddffa37227473eb8b3->enter($__internal_c1dd7c458783cc78429b5b3378d8078450b1639c9962efddffa37227473eb8b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_c1dd7c458783cc78429b5b3378d8078450b1639c9962efddffa37227473eb8b3->leave($__internal_c1dd7c458783cc78429b5b3378d8078450b1639c9962efddffa37227473eb8b3_prof);

        
        $__internal_dc6aa51b4dd1aa8e16327b44fd51cab564718dc5550d9dc055a674e4020167bb->leave($__internal_dc6aa51b4dd1aa8e16327b44fd51cab564718dc5550d9dc055a674e4020167bb_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_2bdd285982af646826ff77af02a0547b8c3a29e54874821e8e0b42bdb925bdba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2bdd285982af646826ff77af02a0547b8c3a29e54874821e8e0b42bdb925bdba->enter($__internal_2bdd285982af646826ff77af02a0547b8c3a29e54874821e8e0b42bdb925bdba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_b16728d78264fe79fff65cb77cbf5ebb5d5d4313c328dbe3e9f4e9fc548c2095 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b16728d78264fe79fff65cb77cbf5ebb5d5d4313c328dbe3e9f4e9fc548c2095->enter($__internal_b16728d78264fe79fff65cb77cbf5ebb5d5d4313c328dbe3e9f4e9fc548c2095_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 141)->display($context);
        
        $__internal_b16728d78264fe79fff65cb77cbf5ebb5d5d4313c328dbe3e9f4e9fc548c2095->leave($__internal_b16728d78264fe79fff65cb77cbf5ebb5d5d4313c328dbe3e9f4e9fc548c2095_prof);

        
        $__internal_2bdd285982af646826ff77af02a0547b8c3a29e54874821e8e0b42bdb925bdba->leave($__internal_2bdd285982af646826ff77af02a0547b8c3a29e54874821e8e0b42bdb925bdba_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "TwigBundle:Exception:exception_full.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
