<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_152be14bf245dde0993fee44ab57b03b6f71fae4ee64224036a160eba6407aa6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bf0a8100e533ac1419653a156f0ea797c917c52e6ecd9f851bb6a42ab267f8a7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bf0a8100e533ac1419653a156f0ea797c917c52e6ecd9f851bb6a42ab267f8a7->enter($__internal_bf0a8100e533ac1419653a156f0ea797c917c52e6ecd9f851bb6a42ab267f8a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        $__internal_6f290e109565aa87d20482454fc3516c596a0ef1a513551e1d1d3e80667fa5b0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6f290e109565aa87d20482454fc3516c596a0ef1a513551e1d1d3e80667fa5b0->enter($__internal_6f290e109565aa87d20482454fc3516c596a0ef1a513551e1d1d3e80667fa5b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_bf0a8100e533ac1419653a156f0ea797c917c52e6ecd9f851bb6a42ab267f8a7->leave($__internal_bf0a8100e533ac1419653a156f0ea797c917c52e6ecd9f851bb6a42ab267f8a7_prof);

        
        $__internal_6f290e109565aa87d20482454fc3516c596a0ef1a513551e1d1d3e80667fa5b0->leave($__internal_6f290e109565aa87d20482454fc3516c596a0ef1a513551e1d1d3e80667fa5b0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
", "@Framework/Form/form_widget.html.php", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget.html.php");
    }
}
