<?php

/* WebProfilerBundle:Profiler:open.html.twig */
class __TwigTemplate_19dfb68d851ddd05e5bd3f446c5cecc8ca30befd0bb86cee9a5c57f648f371d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "WebProfilerBundle:Profiler:open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ce946c60802fb9adf2c34652b76d2ad1c5189360cb866c445f65ca09303340db = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ce946c60802fb9adf2c34652b76d2ad1c5189360cb866c445f65ca09303340db->enter($__internal_ce946c60802fb9adf2c34652b76d2ad1c5189360cb866c445f65ca09303340db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $__internal_ca6d8cb7b693716e78ab9edab75a4df96d5eb16977462293c234f27b8a8f10df = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ca6d8cb7b693716e78ab9edab75a4df96d5eb16977462293c234f27b8a8f10df->enter($__internal_ca6d8cb7b693716e78ab9edab75a4df96d5eb16977462293c234f27b8a8f10df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ce946c60802fb9adf2c34652b76d2ad1c5189360cb866c445f65ca09303340db->leave($__internal_ce946c60802fb9adf2c34652b76d2ad1c5189360cb866c445f65ca09303340db_prof);

        
        $__internal_ca6d8cb7b693716e78ab9edab75a4df96d5eb16977462293c234f27b8a8f10df->leave($__internal_ca6d8cb7b693716e78ab9edab75a4df96d5eb16977462293c234f27b8a8f10df_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_1574b7d4edc16f5cc9d15f164dce6be688bb144df5c2dbf902b419ec7570621d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1574b7d4edc16f5cc9d15f164dce6be688bb144df5c2dbf902b419ec7570621d->enter($__internal_1574b7d4edc16f5cc9d15f164dce6be688bb144df5c2dbf902b419ec7570621d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_a8c7ae1e5d736264f3d9e6e9cbfff158289b5116230c77e3028f59e9502d8275 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a8c7ae1e5d736264f3d9e6e9cbfff158289b5116230c77e3028f59e9502d8275->enter($__internal_a8c7ae1e5d736264f3d9e6e9cbfff158289b5116230c77e3028f59e9502d8275_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_a8c7ae1e5d736264f3d9e6e9cbfff158289b5116230c77e3028f59e9502d8275->leave($__internal_a8c7ae1e5d736264f3d9e6e9cbfff158289b5116230c77e3028f59e9502d8275_prof);

        
        $__internal_1574b7d4edc16f5cc9d15f164dce6be688bb144df5c2dbf902b419ec7570621d->leave($__internal_1574b7d4edc16f5cc9d15f164dce6be688bb144df5c2dbf902b419ec7570621d_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_ebc552fdadab1dc4655266eea4f18e9500f5715c400214d9c58dede73b6bff4f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ebc552fdadab1dc4655266eea4f18e9500f5715c400214d9c58dede73b6bff4f->enter($__internal_ebc552fdadab1dc4655266eea4f18e9500f5715c400214d9c58dede73b6bff4f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_3ac17ac0b075450b1c2fd0d93b546be262d84bebcbe927cf7928ce31674ef7a9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3ac17ac0b075450b1c2fd0d93b546be262d84bebcbe927cf7928ce31674ef7a9->enter($__internal_3ac17ac0b075450b1c2fd0d93b546be262d84bebcbe927cf7928ce31674ef7a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, ($context["file"] ?? $this->getContext($context, "file")), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, ($context["line"] ?? $this->getContext($context, "line")), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt(($context["filename"] ?? $this->getContext($context, "filename")), ($context["line"] ?? $this->getContext($context, "line")),  -1);
        echo "
</div>
";
        
        $__internal_3ac17ac0b075450b1c2fd0d93b546be262d84bebcbe927cf7928ce31674ef7a9->leave($__internal_3ac17ac0b075450b1c2fd0d93b546be262d84bebcbe927cf7928ce31674ef7a9_prof);

        
        $__internal_ebc552fdadab1dc4655266eea4f18e9500f5715c400214d9c58dede73b6bff4f->leave($__internal_ebc552fdadab1dc4655266eea4f18e9500f5715c400214d9c58dede73b6bff4f_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "WebProfilerBundle:Profiler:open.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/open.html.twig");
    }
}
