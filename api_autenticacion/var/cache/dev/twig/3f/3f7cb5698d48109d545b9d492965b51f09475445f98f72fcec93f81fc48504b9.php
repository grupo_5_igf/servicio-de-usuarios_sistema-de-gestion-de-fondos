<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_0c33a369ff4379e84e79ca047a9bd0f8d25a0a2094943a9e180983137ce969ad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_445b0af45cd68ec5b54b5859ac813ebf4b1db5b603e69513c7904c9a29c39004 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_445b0af45cd68ec5b54b5859ac813ebf4b1db5b603e69513c7904c9a29c39004->enter($__internal_445b0af45cd68ec5b54b5859ac813ebf4b1db5b603e69513c7904c9a29c39004_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $__internal_a071870d5206578eebddffea7fbacc9a66084ba2f8eb217969a420ad665d8153 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a071870d5206578eebddffea7fbacc9a66084ba2f8eb217969a420ad665d8153->enter($__internal_a071870d5206578eebddffea7fbacc9a66084ba2f8eb217969a420ad665d8153_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_445b0af45cd68ec5b54b5859ac813ebf4b1db5b603e69513c7904c9a29c39004->leave($__internal_445b0af45cd68ec5b54b5859ac813ebf4b1db5b603e69513c7904c9a29c39004_prof);

        
        $__internal_a071870d5206578eebddffea7fbacc9a66084ba2f8eb217969a420ad665d8153->leave($__internal_a071870d5206578eebddffea7fbacc9a66084ba2f8eb217969a420ad665d8153_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_30a9c456a3fe5dbab684fc9acf9287f665e473f0463d3243cd7aabad1f7921c5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_30a9c456a3fe5dbab684fc9acf9287f665e473f0463d3243cd7aabad1f7921c5->enter($__internal_30a9c456a3fe5dbab684fc9acf9287f665e473f0463d3243cd7aabad1f7921c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_6146e077f226565ba29b5521f6a3c234e12e4cbcdb34e24a2182f0e0f280aa5e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6146e077f226565ba29b5521f6a3c234e12e4cbcdb34e24a2182f0e0f280aa5e->enter($__internal_6146e077f226565ba29b5521f6a3c234e12e4cbcdb34e24a2182f0e0f280aa5e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_6146e077f226565ba29b5521f6a3c234e12e4cbcdb34e24a2182f0e0f280aa5e->leave($__internal_6146e077f226565ba29b5521f6a3c234e12e4cbcdb34e24a2182f0e0f280aa5e_prof);

        
        $__internal_30a9c456a3fe5dbab684fc9acf9287f665e473f0463d3243cd7aabad1f7921c5->leave($__internal_30a9c456a3fe5dbab684fc9acf9287f665e473f0463d3243cd7aabad1f7921c5_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_3f556c1da8008815cdd6a37c1cde01d295380b619d8e84702cf199eae140b4c4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3f556c1da8008815cdd6a37c1cde01d295380b619d8e84702cf199eae140b4c4->enter($__internal_3f556c1da8008815cdd6a37c1cde01d295380b619d8e84702cf199eae140b4c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_1630a94a4e23adf87ae14ed35288756eb5c40ef849e9831f86a56457ba8750fe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1630a94a4e23adf87ae14ed35288756eb5c40ef849e9831f86a56457ba8750fe->enter($__internal_1630a94a4e23adf87ae14ed35288756eb5c40ef849e9831f86a56457ba8750fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_1630a94a4e23adf87ae14ed35288756eb5c40ef849e9831f86a56457ba8750fe->leave($__internal_1630a94a4e23adf87ae14ed35288756eb5c40ef849e9831f86a56457ba8750fe_prof);

        
        $__internal_3f556c1da8008815cdd6a37c1cde01d295380b619d8e84702cf199eae140b4c4->leave($__internal_3f556c1da8008815cdd6a37c1cde01d295380b619d8e84702cf199eae140b4c4_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/toolbar_redirect.html.twig");
    }
}
