<?php

/* :persona:new.html.twig */
class __TwigTemplate_e2911a2304fc0394848412145de40e5b22b8766027d50f31a0a12b31afeb7f10 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":persona:new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b91d0a20f246669f1be6ae6aea60e3113e217f77b55a7f0ef40aa7ab29e1e0d0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b91d0a20f246669f1be6ae6aea60e3113e217f77b55a7f0ef40aa7ab29e1e0d0->enter($__internal_b91d0a20f246669f1be6ae6aea60e3113e217f77b55a7f0ef40aa7ab29e1e0d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":persona:new.html.twig"));

        $__internal_1c129927215b4648018fcf9ea53939c093a722d1ccd04ac4a266f8d6106b8997 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1c129927215b4648018fcf9ea53939c093a722d1ccd04ac4a266f8d6106b8997->enter($__internal_1c129927215b4648018fcf9ea53939c093a722d1ccd04ac4a266f8d6106b8997_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":persona:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b91d0a20f246669f1be6ae6aea60e3113e217f77b55a7f0ef40aa7ab29e1e0d0->leave($__internal_b91d0a20f246669f1be6ae6aea60e3113e217f77b55a7f0ef40aa7ab29e1e0d0_prof);

        
        $__internal_1c129927215b4648018fcf9ea53939c093a722d1ccd04ac4a266f8d6106b8997->leave($__internal_1c129927215b4648018fcf9ea53939c093a722d1ccd04ac4a266f8d6106b8997_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_dfb7625423ef5e3e5baba2ed16723afc0fcd298f834ec07440b348fac67f1410 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dfb7625423ef5e3e5baba2ed16723afc0fcd298f834ec07440b348fac67f1410->enter($__internal_dfb7625423ef5e3e5baba2ed16723afc0fcd298f834ec07440b348fac67f1410_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_6bf4a6d5fedafe4809ef97387fa9e6aa50652859cab0d74671cbd23e621524c0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6bf4a6d5fedafe4809ef97387fa9e6aa50652859cab0d74671cbd23e621524c0->enter($__internal_6bf4a6d5fedafe4809ef97387fa9e6aa50652859cab0d74671cbd23e621524c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Persona creation</h1>

    ";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" />
    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("persona_index");
        echo "\">Back to the list</a>
        </li>
    </ul>
";
        
        $__internal_6bf4a6d5fedafe4809ef97387fa9e6aa50652859cab0d74671cbd23e621524c0->leave($__internal_6bf4a6d5fedafe4809ef97387fa9e6aa50652859cab0d74671cbd23e621524c0_prof);

        
        $__internal_dfb7625423ef5e3e5baba2ed16723afc0fcd298f834ec07440b348fac67f1410->leave($__internal_dfb7625423ef5e3e5baba2ed16723afc0fcd298f834ec07440b348fac67f1410_prof);

    }

    public function getTemplateName()
    {
        return ":persona:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 13,  62 => 9,  57 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Persona creation</h1>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" />
    {{ form_end(form) }}

    <ul>
        <li>
            <a href=\"{{ path('persona_index') }}\">Back to the list</a>
        </li>
    </ul>
{% endblock %}
", ":persona:new.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/app/Resources/views/persona/new.html.twig");
    }
}
