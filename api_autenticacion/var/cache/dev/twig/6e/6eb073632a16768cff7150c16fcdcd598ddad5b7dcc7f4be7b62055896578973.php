<?php

/* :rol:edit.html.twig */
class __TwigTemplate_a4c771e7fe0b6d2229015e6f5e4024147f77149106edac5a7956b6a8b1dd5f8f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":rol:edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_51b661d0a7d53dda2e2968779f878a710ca249df1a8e5471ba9a75bd3bf092e4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_51b661d0a7d53dda2e2968779f878a710ca249df1a8e5471ba9a75bd3bf092e4->enter($__internal_51b661d0a7d53dda2e2968779f878a710ca249df1a8e5471ba9a75bd3bf092e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":rol:edit.html.twig"));

        $__internal_9525c9306b8232c34c25e9f6e4a07d3ab51606c067b3a7542c73cf92a9109bcc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9525c9306b8232c34c25e9f6e4a07d3ab51606c067b3a7542c73cf92a9109bcc->enter($__internal_9525c9306b8232c34c25e9f6e4a07d3ab51606c067b3a7542c73cf92a9109bcc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":rol:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_51b661d0a7d53dda2e2968779f878a710ca249df1a8e5471ba9a75bd3bf092e4->leave($__internal_51b661d0a7d53dda2e2968779f878a710ca249df1a8e5471ba9a75bd3bf092e4_prof);

        
        $__internal_9525c9306b8232c34c25e9f6e4a07d3ab51606c067b3a7542c73cf92a9109bcc->leave($__internal_9525c9306b8232c34c25e9f6e4a07d3ab51606c067b3a7542c73cf92a9109bcc_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_1fc76112a415bbb2010d183a9a30f310cb161536327818e01168c85091fe2bf3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1fc76112a415bbb2010d183a9a30f310cb161536327818e01168c85091fe2bf3->enter($__internal_1fc76112a415bbb2010d183a9a30f310cb161536327818e01168c85091fe2bf3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_ea7cfdfe784b5df061cfb84959478d4632e8f3f452f6c765307a1fe355e93d31 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ea7cfdfe784b5df061cfb84959478d4632e8f3f452f6c765307a1fe355e93d31->enter($__internal_ea7cfdfe784b5df061cfb84959478d4632e8f3f452f6c765307a1fe355e93d31_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Rol edit</h1>

    ";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Edit\" />
    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("rol_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            ";
        // line 16
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 18
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_ea7cfdfe784b5df061cfb84959478d4632e8f3f452f6c765307a1fe355e93d31->leave($__internal_ea7cfdfe784b5df061cfb84959478d4632e8f3f452f6c765307a1fe355e93d31_prof);

        
        $__internal_1fc76112a415bbb2010d183a9a30f310cb161536327818e01168c85091fe2bf3->leave($__internal_1fc76112a415bbb2010d183a9a30f310cb161536327818e01168c85091fe2bf3_prof);

    }

    public function getTemplateName()
    {
        return ":rol:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 18,  75 => 16,  69 => 13,  62 => 9,  57 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Rol edit</h1>

    {{ form_start(edit_form) }}
        {{ form_widget(edit_form) }}
        <input type=\"submit\" value=\"Edit\" />
    {{ form_end(edit_form) }}

    <ul>
        <li>
            <a href=\"{{ path('rol_index') }}\">Back to the list</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", ":rol:edit.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/app/Resources/views/rol/edit.html.twig");
    }
}
