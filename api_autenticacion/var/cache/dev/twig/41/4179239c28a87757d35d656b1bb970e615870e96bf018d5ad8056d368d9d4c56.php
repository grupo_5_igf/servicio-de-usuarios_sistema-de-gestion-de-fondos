<?php

/* :usuario:index.html.twig */
class __TwigTemplate_f8b404977c1da0c0509ed93b45fc23f4addf16c8346a447f240eb5557bb73a90 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":usuario:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_322dd23b0a68541fdf121c62642af81ff27ec401f370f54621b67f6e430a2f0a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_322dd23b0a68541fdf121c62642af81ff27ec401f370f54621b67f6e430a2f0a->enter($__internal_322dd23b0a68541fdf121c62642af81ff27ec401f370f54621b67f6e430a2f0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":usuario:index.html.twig"));

        $__internal_f493b8b32e117ff1d3f1bbcd4a7237bca33e77bd7102a2c6340cfd35ecbe6938 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f493b8b32e117ff1d3f1bbcd4a7237bca33e77bd7102a2c6340cfd35ecbe6938->enter($__internal_f493b8b32e117ff1d3f1bbcd4a7237bca33e77bd7102a2c6340cfd35ecbe6938_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":usuario:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_322dd23b0a68541fdf121c62642af81ff27ec401f370f54621b67f6e430a2f0a->leave($__internal_322dd23b0a68541fdf121c62642af81ff27ec401f370f54621b67f6e430a2f0a_prof);

        
        $__internal_f493b8b32e117ff1d3f1bbcd4a7237bca33e77bd7102a2c6340cfd35ecbe6938->leave($__internal_f493b8b32e117ff1d3f1bbcd4a7237bca33e77bd7102a2c6340cfd35ecbe6938_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_c3f25f7a33edf8a48bb2e367a3b9c9230aba111311ee886b1ec0641e6e554fa2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c3f25f7a33edf8a48bb2e367a3b9c9230aba111311ee886b1ec0641e6e554fa2->enter($__internal_c3f25f7a33edf8a48bb2e367a3b9c9230aba111311ee886b1ec0641e6e554fa2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_0728e4691360857f58052f868a0e4358d705e4b549c9138182f9c9eb94eb70b1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0728e4691360857f58052f868a0e4358d705e4b549c9138182f9c9eb94eb70b1->enter($__internal_0728e4691360857f58052f868a0e4358d705e4b549c9138182f9c9eb94eb70b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Usuarios list</h1>

    <table>
        <thead>
            <tr>
                <th>Idusuario</th>
                <th>Correo</th>
                <th>Contrasenia</th>
                <th>Estado</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["usuarios"] ?? $this->getContext($context, "usuarios")));
        foreach ($context['_seq'] as $context["_key"] => $context["usuario"]) {
            // line 18
            echo "            <tr>
                <td><a href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usuario_show", array("idUsuario" => $this->getAttribute($context["usuario"], "idUsuario", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["usuario"], "idUsuario", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($context["usuario"], "correo", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["usuario"], "contrasenia", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["usuario"], "estado", array()), "html", null, true);
            echo "</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"";
            // line 26
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usuario_show", array("idUsuario" => $this->getAttribute($context["usuario"], "idUsuario", array()))), "html", null, true);
            echo "\">show</a>
                        </li>
                        <li>
                            <a href=\"";
            // line 29
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usuario_edit", array("idUsuario" => $this->getAttribute($context["usuario"], "idUsuario", array()))), "html", null, true);
            echo "\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['usuario'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 40
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usuario_new");
        echo "\">Create a new usuario</a>
        </li>
    </ul>
";
        
        $__internal_0728e4691360857f58052f868a0e4358d705e4b549c9138182f9c9eb94eb70b1->leave($__internal_0728e4691360857f58052f868a0e4358d705e4b549c9138182f9c9eb94eb70b1_prof);

        
        $__internal_c3f25f7a33edf8a48bb2e367a3b9c9230aba111311ee886b1ec0641e6e554fa2->leave($__internal_c3f25f7a33edf8a48bb2e367a3b9c9230aba111311ee886b1ec0641e6e554fa2_prof);

    }

    public function getTemplateName()
    {
        return ":usuario:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 40,  110 => 35,  98 => 29,  92 => 26,  85 => 22,  81 => 21,  77 => 20,  71 => 19,  68 => 18,  64 => 17,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Usuarios list</h1>

    <table>
        <thead>
            <tr>
                <th>Idusuario</th>
                <th>Correo</th>
                <th>Contrasenia</th>
                <th>Estado</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for usuario in usuarios %}
            <tr>
                <td><a href=\"{{ path('usuario_show', { 'idUsuario': usuario.idUsuario }) }}\">{{ usuario.idUsuario }}</a></td>
                <td>{{ usuario.correo }}</td>
                <td>{{ usuario.contrasenia }}</td>
                <td>{{ usuario.estado }}</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"{{ path('usuario_show', { 'idUsuario': usuario.idUsuario }) }}\">show</a>
                        </li>
                        <li>
                            <a href=\"{{ path('usuario_edit', { 'idUsuario': usuario.idUsuario }) }}\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('usuario_new') }}\">Create a new usuario</a>
        </li>
    </ul>
{% endblock %}
", ":usuario:index.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/app/Resources/views/usuario/index.html.twig");
    }
}
