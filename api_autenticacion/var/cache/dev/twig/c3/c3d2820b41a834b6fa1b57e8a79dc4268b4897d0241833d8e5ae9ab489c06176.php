<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_5c060836cae49a47269375db264f99d66a38d16e07ebf891f345d4cc2de7bff2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0dc6d54ab3daedfb8323b4191727b54f43c37aa21683ba28493c601debf8f4f9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0dc6d54ab3daedfb8323b4191727b54f43c37aa21683ba28493c601debf8f4f9->enter($__internal_0dc6d54ab3daedfb8323b4191727b54f43c37aa21683ba28493c601debf8f4f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        $__internal_351fbc3c120a017d69319a88bc399bac6a2e5fd60e9b882fbbbf2eb653477f0d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_351fbc3c120a017d69319a88bc399bac6a2e5fd60e9b882fbbbf2eb653477f0d->enter($__internal_351fbc3c120a017d69319a88bc399bac6a2e5fd60e9b882fbbbf2eb653477f0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_0dc6d54ab3daedfb8323b4191727b54f43c37aa21683ba28493c601debf8f4f9->leave($__internal_0dc6d54ab3daedfb8323b4191727b54f43c37aa21683ba28493c601debf8f4f9_prof);

        
        $__internal_351fbc3c120a017d69319a88bc399bac6a2e5fd60e9b882fbbbf2eb653477f0d->leave($__internal_351fbc3c120a017d69319a88bc399bac6a2e5fd60e9b882fbbbf2eb653477f0d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
", "@Framework/Form/choice_widget.html.php", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget.html.php");
    }
}
