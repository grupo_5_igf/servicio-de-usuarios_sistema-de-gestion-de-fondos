<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_41beac728198993731c562523c12cd3b20e7bb3d2627b8faff2ebf8f3db45573 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_33de4e8f1bc9714965151d0f23a667bceed1244130a295cd706b27ea42be4953 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_33de4e8f1bc9714965151d0f23a667bceed1244130a295cd706b27ea42be4953->enter($__internal_33de4e8f1bc9714965151d0f23a667bceed1244130a295cd706b27ea42be4953_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        $__internal_baf32c020f626a5ce3663ec1e31cc3298d8252ceec05f67dba29dd0ef37d1a12 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_baf32c020f626a5ce3663ec1e31cc3298d8252ceec05f67dba29dd0ef37d1a12->enter($__internal_baf32c020f626a5ce3663ec1e31cc3298d8252ceec05f67dba29dd0ef37d1a12_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_33de4e8f1bc9714965151d0f23a667bceed1244130a295cd706b27ea42be4953->leave($__internal_33de4e8f1bc9714965151d0f23a667bceed1244130a295cd706b27ea42be4953_prof);

        
        $__internal_baf32c020f626a5ce3663ec1e31cc3298d8252ceec05f67dba29dd0ef37d1a12->leave($__internal_baf32c020f626a5ce3663ec1e31cc3298d8252ceec05f67dba29dd0ef37d1a12_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
", "@Framework/Form/collection_widget.html.php", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/collection_widget.html.php");
    }
}
