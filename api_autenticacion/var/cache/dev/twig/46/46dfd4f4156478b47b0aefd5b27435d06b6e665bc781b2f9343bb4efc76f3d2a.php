<?php

/* TwigBundle::layout.html.twig */
class __TwigTemplate_40d17f889cda0e9198fdd0de6a45002f4e95d20d7f1dda71b6e52967dec960e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_517e8f81fc46e2dc650ac9def2d373df663cdac7893c1eea00e99ea92eba7c60 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_517e8f81fc46e2dc650ac9def2d373df663cdac7893c1eea00e99ea92eba7c60->enter($__internal_517e8f81fc46e2dc650ac9def2d373df663cdac7893c1eea00e99ea92eba7c60_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle::layout.html.twig"));

        $__internal_1e25184ffbf57166b8c1ebd7d0b1df275f266b84e9fef7d2d8db4ff7aa5cca76 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1e25184ffbf57166b8c1ebd7d0b1df275f266b84e9fef7d2d8db4ff7aa5cca76->enter($__internal_1e25184ffbf57166b8c1ebd7d0b1df275f266b84e9fef7d2d8db4ff7aa5cca76_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle::layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_517e8f81fc46e2dc650ac9def2d373df663cdac7893c1eea00e99ea92eba7c60->leave($__internal_517e8f81fc46e2dc650ac9def2d373df663cdac7893c1eea00e99ea92eba7c60_prof);

        
        $__internal_1e25184ffbf57166b8c1ebd7d0b1df275f266b84e9fef7d2d8db4ff7aa5cca76->leave($__internal_1e25184ffbf57166b8c1ebd7d0b1df275f266b84e9fef7d2d8db4ff7aa5cca76_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_cca1d8c9cc593363efe34b788dcca04777fa6158f78719aa88bd7e63f236ad94 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cca1d8c9cc593363efe34b788dcca04777fa6158f78719aa88bd7e63f236ad94->enter($__internal_cca1d8c9cc593363efe34b788dcca04777fa6158f78719aa88bd7e63f236ad94_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_1dec862706300e98e80e0cb3de8c9c7e60ca4838b8836fc13e474f61fac00445 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1dec862706300e98e80e0cb3de8c9c7e60ca4838b8836fc13e474f61fac00445->enter($__internal_1dec862706300e98e80e0cb3de8c9c7e60ca4838b8836fc13e474f61fac00445_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_1dec862706300e98e80e0cb3de8c9c7e60ca4838b8836fc13e474f61fac00445->leave($__internal_1dec862706300e98e80e0cb3de8c9c7e60ca4838b8836fc13e474f61fac00445_prof);

        
        $__internal_cca1d8c9cc593363efe34b788dcca04777fa6158f78719aa88bd7e63f236ad94->leave($__internal_cca1d8c9cc593363efe34b788dcca04777fa6158f78719aa88bd7e63f236ad94_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_8382a8453154fbc213bfd33bf09c4b6ab275b467cbec519a6a3fa96b5c765fbe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8382a8453154fbc213bfd33bf09c4b6ab275b467cbec519a6a3fa96b5c765fbe->enter($__internal_8382a8453154fbc213bfd33bf09c4b6ab275b467cbec519a6a3fa96b5c765fbe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_81887bb154ab1365f8e8e58ad7b0b8f950cdc165719efd2929add626b6991798 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_81887bb154ab1365f8e8e58ad7b0b8f950cdc165719efd2929add626b6991798->enter($__internal_81887bb154ab1365f8e8e58ad7b0b8f950cdc165719efd2929add626b6991798_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_81887bb154ab1365f8e8e58ad7b0b8f950cdc165719efd2929add626b6991798->leave($__internal_81887bb154ab1365f8e8e58ad7b0b8f950cdc165719efd2929add626b6991798_prof);

        
        $__internal_8382a8453154fbc213bfd33bf09c4b6ab275b467cbec519a6a3fa96b5c765fbe->leave($__internal_8382a8453154fbc213bfd33bf09c4b6ab275b467cbec519a6a3fa96b5c765fbe_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_41392fa73ba98a6fa3e9603c77e09f655064695d9dd944fdf653c451f6264621 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_41392fa73ba98a6fa3e9603c77e09f655064695d9dd944fdf653c451f6264621->enter($__internal_41392fa73ba98a6fa3e9603c77e09f655064695d9dd944fdf653c451f6264621_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_d11acc7f9caef24911ad6f6205c86d17a7c046785da056499d4f26ae7a316f6f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d11acc7f9caef24911ad6f6205c86d17a7c046785da056499d4f26ae7a316f6f->enter($__internal_d11acc7f9caef24911ad6f6205c86d17a7c046785da056499d4f26ae7a316f6f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_d11acc7f9caef24911ad6f6205c86d17a7c046785da056499d4f26ae7a316f6f->leave($__internal_d11acc7f9caef24911ad6f6205c86d17a7c046785da056499d4f26ae7a316f6f_prof);

        
        $__internal_41392fa73ba98a6fa3e9603c77e09f655064695d9dd944fdf653c451f6264621->leave($__internal_41392fa73ba98a6fa3e9603c77e09f655064695d9dd944fdf653c451f6264621_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "TwigBundle::layout.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/layout.html.twig");
    }
}
