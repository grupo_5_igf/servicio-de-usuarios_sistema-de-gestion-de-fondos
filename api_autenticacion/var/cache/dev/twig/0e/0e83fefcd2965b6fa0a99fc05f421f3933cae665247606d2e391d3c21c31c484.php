<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_175b6ce867f5a90d57d065b2e25b22389265e3d6d895c4b5f249ac7ce76d5f66 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_52b4d303ae93aaef19be3eb0b0fab2532975e9234046ee0393efd11166137035 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_52b4d303ae93aaef19be3eb0b0fab2532975e9234046ee0393efd11166137035->enter($__internal_52b4d303ae93aaef19be3eb0b0fab2532975e9234046ee0393efd11166137035_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        $__internal_e98981ae69145dd3dd4416a3c62b9eb10cba6df406bdcb31887c620a80e1bad0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e98981ae69145dd3dd4416a3c62b9eb10cba6df406bdcb31887c620a80e1bad0->enter($__internal_e98981ae69145dd3dd4416a3c62b9eb10cba6df406bdcb31887c620a80e1bad0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_52b4d303ae93aaef19be3eb0b0fab2532975e9234046ee0393efd11166137035->leave($__internal_52b4d303ae93aaef19be3eb0b0fab2532975e9234046ee0393efd11166137035_prof);

        
        $__internal_e98981ae69145dd3dd4416a3c62b9eb10cba6df406bdcb31887c620a80e1bad0->leave($__internal_e98981ae69145dd3dd4416a3c62b9eb10cba6df406bdcb31887c620a80e1bad0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
", "@Framework/Form/form.html.php", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form.html.php");
    }
}
