<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_a2b079f00ea46b0ca523b1c68a813c5faf2b958c961375da2754bf79afa549a0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f636bf4bb6eda16ab5d6123b645ac848b9d687310227e4586723791574d1f565 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f636bf4bb6eda16ab5d6123b645ac848b9d687310227e4586723791574d1f565->enter($__internal_f636bf4bb6eda16ab5d6123b645ac848b9d687310227e4586723791574d1f565_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        $__internal_b7fd089faeda939a3ddebb248cea52360f92f23bf2b0784cb831c42d5f703e2d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b7fd089faeda939a3ddebb248cea52360f92f23bf2b0784cb831c42d5f703e2d->enter($__internal_b7fd089faeda939a3ddebb248cea52360f92f23bf2b0784cb831c42d5f703e2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_f636bf4bb6eda16ab5d6123b645ac848b9d687310227e4586723791574d1f565->leave($__internal_f636bf4bb6eda16ab5d6123b645ac848b9d687310227e4586723791574d1f565_prof);

        
        $__internal_b7fd089faeda939a3ddebb248cea52360f92f23bf2b0784cb831c42d5f703e2d->leave($__internal_b7fd089faeda939a3ddebb248cea52360f92f23bf2b0784cb831c42d5f703e2d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
", "@Framework/Form/search_widget.html.php", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/search_widget.html.php");
    }
}
