<?php

/* :rol:index.html.twig */
class __TwigTemplate_ff8cca92ad7fa81b5057a09fe40937740c409691150cb2ea618cb1453360db26 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":rol:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ce257c4b1892b6d8348e47d3e0a0a8fde26b5d96c3de7b42291a4eae59b5f082 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ce257c4b1892b6d8348e47d3e0a0a8fde26b5d96c3de7b42291a4eae59b5f082->enter($__internal_ce257c4b1892b6d8348e47d3e0a0a8fde26b5d96c3de7b42291a4eae59b5f082_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":rol:index.html.twig"));

        $__internal_455b10849a76ecdcbeaf6908c73da732de9db4e17c033155d01965b220530714 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_455b10849a76ecdcbeaf6908c73da732de9db4e17c033155d01965b220530714->enter($__internal_455b10849a76ecdcbeaf6908c73da732de9db4e17c033155d01965b220530714_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":rol:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ce257c4b1892b6d8348e47d3e0a0a8fde26b5d96c3de7b42291a4eae59b5f082->leave($__internal_ce257c4b1892b6d8348e47d3e0a0a8fde26b5d96c3de7b42291a4eae59b5f082_prof);

        
        $__internal_455b10849a76ecdcbeaf6908c73da732de9db4e17c033155d01965b220530714->leave($__internal_455b10849a76ecdcbeaf6908c73da732de9db4e17c033155d01965b220530714_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_941c20351bb2c3ed31d3c5817f060da76b1b6fc6f76e377331200b6fedcf4c9b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_941c20351bb2c3ed31d3c5817f060da76b1b6fc6f76e377331200b6fedcf4c9b->enter($__internal_941c20351bb2c3ed31d3c5817f060da76b1b6fc6f76e377331200b6fedcf4c9b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_fea635560cb0a3543f3fa73ca58e99abb298da49e7b61d270c526d21c477e1c4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fea635560cb0a3543f3fa73ca58e99abb298da49e7b61d270c526d21c477e1c4->enter($__internal_fea635560cb0a3543f3fa73ca58e99abb298da49e7b61d270c526d21c477e1c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Rols list</h1>

    <table>
        <thead>
            <tr>
                <th>Idrol</th>
                <th>Nombrerol</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["rols"] ?? $this->getContext($context, "rols")));
        foreach ($context['_seq'] as $context["_key"] => $context["rol"]) {
            // line 16
            echo "            <tr>
                <td><a href=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("rol_show", array("idRol" => $this->getAttribute($context["rol"], "idRol", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["rol"], "idRol", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["rol"], "nombreRol", array()), "html", null, true);
            echo "</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("rol_show", array("idRol" => $this->getAttribute($context["rol"], "idRol", array()))), "html", null, true);
            echo "\">show</a>
                        </li>
                        <li>
                            <a href=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("rol_edit", array("idRol" => $this->getAttribute($context["rol"], "idRol", array()))), "html", null, true);
            echo "\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rol'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 36
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("rol_new");
        echo "\">Create a new rol</a>
        </li>
    </ul>
";
        
        $__internal_fea635560cb0a3543f3fa73ca58e99abb298da49e7b61d270c526d21c477e1c4->leave($__internal_fea635560cb0a3543f3fa73ca58e99abb298da49e7b61d270c526d21c477e1c4_prof);

        
        $__internal_941c20351bb2c3ed31d3c5817f060da76b1b6fc6f76e377331200b6fedcf4c9b->leave($__internal_941c20351bb2c3ed31d3c5817f060da76b1b6fc6f76e377331200b6fedcf4c9b_prof);

    }

    public function getTemplateName()
    {
        return ":rol:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 36,  100 => 31,  88 => 25,  82 => 22,  75 => 18,  69 => 17,  66 => 16,  62 => 15,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Rols list</h1>

    <table>
        <thead>
            <tr>
                <th>Idrol</th>
                <th>Nombrerol</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for rol in rols %}
            <tr>
                <td><a href=\"{{ path('rol_show', { 'idRol': rol.idRol }) }}\">{{ rol.idRol }}</a></td>
                <td>{{ rol.nombreRol }}</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"{{ path('rol_show', { 'idRol': rol.idRol }) }}\">show</a>
                        </li>
                        <li>
                            <a href=\"{{ path('rol_edit', { 'idRol': rol.idRol }) }}\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('rol_new') }}\">Create a new rol</a>
        </li>
    </ul>
{% endblock %}
", ":rol:index.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/app/Resources/views/rol/index.html.twig");
    }
}
