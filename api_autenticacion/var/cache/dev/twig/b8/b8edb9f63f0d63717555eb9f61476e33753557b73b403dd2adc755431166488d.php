<?php

/* WebProfilerBundle:Profiler:info.html.twig */
class __TwigTemplate_5fc722d62ca28f285b606fe786535b6a2c11de849e5659f523da67b1533eb6bc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Profiler:info.html.twig", 1);
        $this->blocks = array(
            'summary' => array($this, 'block_summary'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4ff960bf8959539e2c0b3dd2a7f7b8e2058a3ed57c4542a0cfae70b19730368a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4ff960bf8959539e2c0b3dd2a7f7b8e2058a3ed57c4542a0cfae70b19730368a->enter($__internal_4ff960bf8959539e2c0b3dd2a7f7b8e2058a3ed57c4542a0cfae70b19730368a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        $__internal_bc9e294f4073df38f984096e579a5442c6ed0526a181f2b860f5d83e0f38c3e0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bc9e294f4073df38f984096e579a5442c6ed0526a181f2b860f5d83e0f38c3e0->enter($__internal_bc9e294f4073df38f984096e579a5442c6ed0526a181f2b860f5d83e0f38c3e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        // line 3
        $context["messages"] = array("no_token" => array("status" => "error", "title" => (((((        // line 6
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("There are no profiles") : ("Token not found")), "message" => (((((        // line 7
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("No profiles found in the database.") : ((("Token \"" . ((array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : (""))) . "\" was not found in the database.")))));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4ff960bf8959539e2c0b3dd2a7f7b8e2058a3ed57c4542a0cfae70b19730368a->leave($__internal_4ff960bf8959539e2c0b3dd2a7f7b8e2058a3ed57c4542a0cfae70b19730368a_prof);

        
        $__internal_bc9e294f4073df38f984096e579a5442c6ed0526a181f2b860f5d83e0f38c3e0->leave($__internal_bc9e294f4073df38f984096e579a5442c6ed0526a181f2b860f5d83e0f38c3e0_prof);

    }

    // line 11
    public function block_summary($context, array $blocks = array())
    {
        $__internal_e8e1168dcf964c874f9a44d7f73438a219533f1d66e2819b6ec44a290279f4df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e8e1168dcf964c874f9a44d7f73438a219533f1d66e2819b6ec44a290279f4df->enter($__internal_e8e1168dcf964c874f9a44d7f73438a219533f1d66e2819b6ec44a290279f4df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        $__internal_b95e032603f713314543caa9897fda738d4c0ed014b8034929b570778337a543 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b95e032603f713314543caa9897fda738d4c0ed014b8034929b570778337a543->enter($__internal_b95e032603f713314543caa9897fda738d4c0ed014b8034929b570778337a543_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        // line 12
        echo "    <div class=\"status status-";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array()), "html", null, true);
        echo "\">
        <div class=\"container\">
            <h2>";
        // line 14
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array())), "html", null, true);
        echo "</h2>
        </div>
    </div>
";
        
        $__internal_b95e032603f713314543caa9897fda738d4c0ed014b8034929b570778337a543->leave($__internal_b95e032603f713314543caa9897fda738d4c0ed014b8034929b570778337a543_prof);

        
        $__internal_e8e1168dcf964c874f9a44d7f73438a219533f1d66e2819b6ec44a290279f4df->leave($__internal_e8e1168dcf964c874f9a44d7f73438a219533f1d66e2819b6ec44a290279f4df_prof);

    }

    // line 19
    public function block_panel($context, array $blocks = array())
    {
        $__internal_c2ef4331ccec17f0c252c9256d9f4f5a9eff8a699804f864cf34d331261ee477 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c2ef4331ccec17f0c252c9256d9f4f5a9eff8a699804f864cf34d331261ee477->enter($__internal_c2ef4331ccec17f0c252c9256d9f4f5a9eff8a699804f864cf34d331261ee477_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_96fcc032089d3c0b67c0b1c822c44c8dd8556c4c5beeb194a1c379eb256dca0d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_96fcc032089d3c0b67c0b1c822c44c8dd8556c4c5beeb194a1c379eb256dca0d->enter($__internal_96fcc032089d3c0b67c0b1c822c44c8dd8556c4c5beeb194a1c379eb256dca0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 20
        echo "    <h2>";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "title", array()), "html", null, true);
        echo "</h2>
    <p>";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "message", array()), "html", null, true);
        echo "</p>
";
        
        $__internal_96fcc032089d3c0b67c0b1c822c44c8dd8556c4c5beeb194a1c379eb256dca0d->leave($__internal_96fcc032089d3c0b67c0b1c822c44c8dd8556c4c5beeb194a1c379eb256dca0d_prof);

        
        $__internal_c2ef4331ccec17f0c252c9256d9f4f5a9eff8a699804f864cf34d331261ee477->leave($__internal_c2ef4331ccec17f0c252c9256d9f4f5a9eff8a699804f864cf34d331261ee477_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 21,  84 => 20,  75 => 19,  61 => 14,  55 => 12,  46 => 11,  36 => 1,  34 => 7,  33 => 6,  32 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% set messages = {
    'no_token' : {
        status:  'error',
        title:   (token|default('') == 'latest') ? 'There are no profiles' : 'Token not found',
        message: (token|default('') == 'latest') ? 'No profiles found in the database.' : 'Token \"' ~ token|default('') ~ '\" was not found in the database.'
    }
} %}

{% block summary %}
    <div class=\"status status-{{ messages[about].status }}\">
        <div class=\"container\">
            <h2>{{ messages[about].status|title }}</h2>
        </div>
    </div>
{% endblock %}

{% block panel %}
    <h2>{{ messages[about].title }}</h2>
    <p>{{ messages[about].message }}</p>
{% endblock %}
", "WebProfilerBundle:Profiler:info.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/info.html.twig");
    }
}
