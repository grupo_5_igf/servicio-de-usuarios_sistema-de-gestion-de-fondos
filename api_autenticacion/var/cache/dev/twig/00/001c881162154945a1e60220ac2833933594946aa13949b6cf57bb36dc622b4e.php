<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_295cf4d1232de5526216ca5e347ec33fadab247a9776f06a354337f61068f495 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ef73e4d2b97f2300cf86cc7aace941737222864bbd28e4c6863bd4a1170aa5b5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ef73e4d2b97f2300cf86cc7aace941737222864bbd28e4c6863bd4a1170aa5b5->enter($__internal_ef73e4d2b97f2300cf86cc7aace941737222864bbd28e4c6863bd4a1170aa5b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        $__internal_dab9d2fb2bbcd4e80d450428f93f7010099141251a2bcabc76d5d9a4084f6525 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dab9d2fb2bbcd4e80d450428f93f7010099141251a2bcabc76d5d9a4084f6525->enter($__internal_dab9d2fb2bbcd4e80d450428f93f7010099141251a2bcabc76d5d9a4084f6525_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_ef73e4d2b97f2300cf86cc7aace941737222864bbd28e4c6863bd4a1170aa5b5->leave($__internal_ef73e4d2b97f2300cf86cc7aace941737222864bbd28e4c6863bd4a1170aa5b5_prof);

        
        $__internal_dab9d2fb2bbcd4e80d450428f93f7010099141251a2bcabc76d5d9a4084f6525->leave($__internal_dab9d2fb2bbcd4e80d450428f93f7010099141251a2bcabc76d5d9a4084f6525_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/button_row.html.php", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_row.html.php");
    }
}
