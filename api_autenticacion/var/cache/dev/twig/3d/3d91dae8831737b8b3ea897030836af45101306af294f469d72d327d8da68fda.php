<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_47253a0760319c2c9cf444bba706c07dc8eabaf3256444a8438258b724cc385c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2d8caa9776be097bcf5e9f64e2175ec9755311d9fffef78fd5c809166f15161a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2d8caa9776be097bcf5e9f64e2175ec9755311d9fffef78fd5c809166f15161a->enter($__internal_2d8caa9776be097bcf5e9f64e2175ec9755311d9fffef78fd5c809166f15161a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        $__internal_2a9acf718825d39a701d1a01dd3ad6e5e92d4146a5ea0474547d3dc5f58dc419 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2a9acf718825d39a701d1a01dd3ad6e5e92d4146a5ea0474547d3dc5f58dc419->enter($__internal_2a9acf718825d39a701d1a01dd3ad6e5e92d4146a5ea0474547d3dc5f58dc419_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_2d8caa9776be097bcf5e9f64e2175ec9755311d9fffef78fd5c809166f15161a->leave($__internal_2d8caa9776be097bcf5e9f64e2175ec9755311d9fffef78fd5c809166f15161a_prof);

        
        $__internal_2a9acf718825d39a701d1a01dd3ad6e5e92d4146a5ea0474547d3dc5f58dc419->leave($__internal_2a9acf718825d39a701d1a01dd3ad6e5e92d4146a5ea0474547d3dc5f58dc419_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "TwigBundle:Exception:error.css.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.css.twig");
    }
}
