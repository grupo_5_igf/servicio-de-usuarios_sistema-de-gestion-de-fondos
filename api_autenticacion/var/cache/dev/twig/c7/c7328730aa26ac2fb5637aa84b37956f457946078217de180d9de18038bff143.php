<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_440052636673501027f2ccee8006eb2f949d3bfff4de277fc05e94b3b877842d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0a9cf2fef43899ffa4ed3585ef943ca339672e96c4eefa3472eb707cfb4cca47 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0a9cf2fef43899ffa4ed3585ef943ca339672e96c4eefa3472eb707cfb4cca47->enter($__internal_0a9cf2fef43899ffa4ed3585ef943ca339672e96c4eefa3472eb707cfb4cca47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        $__internal_1dba198a6213d1e20ba829903dc183b6365886ef31707f93c29e028cbb0dd02c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1dba198a6213d1e20ba829903dc183b6365886ef31707f93c29e028cbb0dd02c->enter($__internal_1dba198a6213d1e20ba829903dc183b6365886ef31707f93c29e028cbb0dd02c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => ($context["status_code"] ?? $this->getContext($context, "status_code")), "message" => ($context["status_text"] ?? $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_0a9cf2fef43899ffa4ed3585ef943ca339672e96c4eefa3472eb707cfb4cca47->leave($__internal_0a9cf2fef43899ffa4ed3585ef943ca339672e96c4eefa3472eb707cfb4cca47_prof);

        
        $__internal_1dba198a6213d1e20ba829903dc183b6365886ef31707f93c29e028cbb0dd02c->leave($__internal_1dba198a6213d1e20ba829903dc183b6365886ef31707f93c29e028cbb0dd02c_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}
", "TwigBundle:Exception:error.json.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.json.twig");
    }
}
