<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_c23ca9f22ed00e48e7dc54bb4272e9abad87680fc001638199a83a6562b06463 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7b72adb86e89a3cdd9e9c4fabc57e3af27aa587f8650b45e312db0c17c56426c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7b72adb86e89a3cdd9e9c4fabc57e3af27aa587f8650b45e312db0c17c56426c->enter($__internal_7b72adb86e89a3cdd9e9c4fabc57e3af27aa587f8650b45e312db0c17c56426c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        $__internal_efb8b40b986a653ea4433b8b867f48165b772a65a6b865e1a51c6ad58f43252e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_efb8b40b986a653ea4433b8b867f48165b772a65a6b865e1a51c6ad58f43252e->enter($__internal_efb8b40b986a653ea4433b8b867f48165b772a65a6b865e1a51c6ad58f43252e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_7b72adb86e89a3cdd9e9c4fabc57e3af27aa587f8650b45e312db0c17c56426c->leave($__internal_7b72adb86e89a3cdd9e9c4fabc57e3af27aa587f8650b45e312db0c17c56426c_prof);

        
        $__internal_efb8b40b986a653ea4433b8b867f48165b772a65a6b865e1a51c6ad58f43252e->leave($__internal_efb8b40b986a653ea4433b8b867f48165b772a65a6b865e1a51c6ad58f43252e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
", "@Framework/Form/container_attributes.html.php", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/container_attributes.html.php");
    }
}
