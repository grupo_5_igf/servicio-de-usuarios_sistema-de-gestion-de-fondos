<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_c8948eab21f13e8d1a5944c9aa6a68b35a475a21ebdbde87d9199569f7b41d48 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_217d7c06e6d7647979e25af7da8391b27b07efea28dce7b00d68b00bb7a87bc1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_217d7c06e6d7647979e25af7da8391b27b07efea28dce7b00d68b00bb7a87bc1->enter($__internal_217d7c06e6d7647979e25af7da8391b27b07efea28dce7b00d68b00bb7a87bc1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        $__internal_831acebe2cc62d55139a51b9338469dcdc77babca566fa17b8f40b920c592653 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_831acebe2cc62d55139a51b9338469dcdc77babca566fa17b8f40b920c592653->enter($__internal_831acebe2cc62d55139a51b9338469dcdc77babca566fa17b8f40b920c592653_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_217d7c06e6d7647979e25af7da8391b27b07efea28dce7b00d68b00bb7a87bc1->leave($__internal_217d7c06e6d7647979e25af7da8391b27b07efea28dce7b00d68b00bb7a87bc1_prof);

        
        $__internal_831acebe2cc62d55139a51b9338469dcdc77babca566fa17b8f40b920c592653->leave($__internal_831acebe2cc62d55139a51b9338469dcdc77babca566fa17b8f40b920c592653_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->widget(\$form) ?>
", "@Framework/Form/hidden_row.html.php", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_row.html.php");
    }
}
