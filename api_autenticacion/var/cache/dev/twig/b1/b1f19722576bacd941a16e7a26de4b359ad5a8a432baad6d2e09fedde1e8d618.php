<?php

/* base.html.twig */
class __TwigTemplate_e7512925d8fb00307cc049a1006d437f3028b1723b810d6e2acede88a13e1891 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6682c3654dec1345e6d0f142d020e06e1c2aca963963403145a96013bd04cb26 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6682c3654dec1345e6d0f142d020e06e1c2aca963963403145a96013bd04cb26->enter($__internal_6682c3654dec1345e6d0f142d020e06e1c2aca963963403145a96013bd04cb26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_34db229ed04b9616230f3c5b3c2e8a2cc7b09cdae27ab295bcb43d5b5be4835f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_34db229ed04b9616230f3c5b3c2e8a2cc7b09cdae27ab295bcb43d5b5be4835f->enter($__internal_34db229ed04b9616230f3c5b3c2e8a2cc7b09cdae27ab295bcb43d5b5be4835f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_6682c3654dec1345e6d0f142d020e06e1c2aca963963403145a96013bd04cb26->leave($__internal_6682c3654dec1345e6d0f142d020e06e1c2aca963963403145a96013bd04cb26_prof);

        
        $__internal_34db229ed04b9616230f3c5b3c2e8a2cc7b09cdae27ab295bcb43d5b5be4835f->leave($__internal_34db229ed04b9616230f3c5b3c2e8a2cc7b09cdae27ab295bcb43d5b5be4835f_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_179ed1635a1fdc31e4ef5cfb91c4da43dbaa7cc388b22746b40ef11f6447fa88 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_179ed1635a1fdc31e4ef5cfb91c4da43dbaa7cc388b22746b40ef11f6447fa88->enter($__internal_179ed1635a1fdc31e4ef5cfb91c4da43dbaa7cc388b22746b40ef11f6447fa88_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_88094b8d9745f9aca3a2bcd1c38c8c0b22919ee8f745634552a706870fd09969 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_88094b8d9745f9aca3a2bcd1c38c8c0b22919ee8f745634552a706870fd09969->enter($__internal_88094b8d9745f9aca3a2bcd1c38c8c0b22919ee8f745634552a706870fd09969_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_88094b8d9745f9aca3a2bcd1c38c8c0b22919ee8f745634552a706870fd09969->leave($__internal_88094b8d9745f9aca3a2bcd1c38c8c0b22919ee8f745634552a706870fd09969_prof);

        
        $__internal_179ed1635a1fdc31e4ef5cfb91c4da43dbaa7cc388b22746b40ef11f6447fa88->leave($__internal_179ed1635a1fdc31e4ef5cfb91c4da43dbaa7cc388b22746b40ef11f6447fa88_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_2717121085e75d641b3cf76b92d5e2be66d2c40b85c57164f860c57927af9857 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2717121085e75d641b3cf76b92d5e2be66d2c40b85c57164f860c57927af9857->enter($__internal_2717121085e75d641b3cf76b92d5e2be66d2c40b85c57164f860c57927af9857_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_695f38d96c6d627e8a7bb7068535eca5a07282b6f9e8bf33bb48ceb737c71866 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_695f38d96c6d627e8a7bb7068535eca5a07282b6f9e8bf33bb48ceb737c71866->enter($__internal_695f38d96c6d627e8a7bb7068535eca5a07282b6f9e8bf33bb48ceb737c71866_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_695f38d96c6d627e8a7bb7068535eca5a07282b6f9e8bf33bb48ceb737c71866->leave($__internal_695f38d96c6d627e8a7bb7068535eca5a07282b6f9e8bf33bb48ceb737c71866_prof);

        
        $__internal_2717121085e75d641b3cf76b92d5e2be66d2c40b85c57164f860c57927af9857->leave($__internal_2717121085e75d641b3cf76b92d5e2be66d2c40b85c57164f860c57927af9857_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_aa396e172a67e7b54e783d6504181cb7167990d38633d617d74d35f27b484099 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aa396e172a67e7b54e783d6504181cb7167990d38633d617d74d35f27b484099->enter($__internal_aa396e172a67e7b54e783d6504181cb7167990d38633d617d74d35f27b484099_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_b2746e26f6cc038abbcb3d3b86d9aa88fd878e25796715c606954a3b93f8a571 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b2746e26f6cc038abbcb3d3b86d9aa88fd878e25796715c606954a3b93f8a571->enter($__internal_b2746e26f6cc038abbcb3d3b86d9aa88fd878e25796715c606954a3b93f8a571_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_b2746e26f6cc038abbcb3d3b86d9aa88fd878e25796715c606954a3b93f8a571->leave($__internal_b2746e26f6cc038abbcb3d3b86d9aa88fd878e25796715c606954a3b93f8a571_prof);

        
        $__internal_aa396e172a67e7b54e783d6504181cb7167990d38633d617d74d35f27b484099->leave($__internal_aa396e172a67e7b54e783d6504181cb7167990d38633d617d74d35f27b484099_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_671e67f0d4cc0c273a767c3a5f24c7f340c201911989b0b99a242f2fb2694829 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_671e67f0d4cc0c273a767c3a5f24c7f340c201911989b0b99a242f2fb2694829->enter($__internal_671e67f0d4cc0c273a767c3a5f24c7f340c201911989b0b99a242f2fb2694829_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_8580075df6e6c56831ea9682c1aa3e2c8bf5caef633c7f683d194e8200f9dfd6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8580075df6e6c56831ea9682c1aa3e2c8bf5caef633c7f683d194e8200f9dfd6->enter($__internal_8580075df6e6c56831ea9682c1aa3e2c8bf5caef633c7f683d194e8200f9dfd6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_8580075df6e6c56831ea9682c1aa3e2c8bf5caef633c7f683d194e8200f9dfd6->leave($__internal_8580075df6e6c56831ea9682c1aa3e2c8bf5caef633c7f683d194e8200f9dfd6_prof);

        
        $__internal_671e67f0d4cc0c273a767c3a5f24c7f340c201911989b0b99a242f2fb2694829->leave($__internal_671e67f0d4cc0c273a767c3a5f24c7f340c201911989b0b99a242f2fb2694829_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 11,  100 => 10,  83 => 6,  65 => 5,  53 => 12,  50 => 11,  48 => 10,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/app/Resources/views/base.html.twig");
    }
}
