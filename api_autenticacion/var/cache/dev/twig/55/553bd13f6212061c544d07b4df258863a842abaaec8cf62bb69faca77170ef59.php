<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_102b9f0600ae7dffa1e6cfef2661902e0f33827312a7b37369632ebb95f503d5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_246b72bfb289b58d701b7909d2d233b026f9753eaa5943eb5aec6faee71caec0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_246b72bfb289b58d701b7909d2d233b026f9753eaa5943eb5aec6faee71caec0->enter($__internal_246b72bfb289b58d701b7909d2d233b026f9753eaa5943eb5aec6faee71caec0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        $__internal_9f436fd90d4ddf6d7b50ad8b47b618db5006a89cdecd1d6e65f8b9f32b803947 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9f436fd90d4ddf6d7b50ad8b47b618db5006a89cdecd1d6e65f8b9f32b803947->enter($__internal_9f436fd90d4ddf6d7b50ad8b47b618db5006a89cdecd1d6e65f8b9f32b803947_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_246b72bfb289b58d701b7909d2d233b026f9753eaa5943eb5aec6faee71caec0->leave($__internal_246b72bfb289b58d701b7909d2d233b026f9753eaa5943eb5aec6faee71caec0_prof);

        
        $__internal_9f436fd90d4ddf6d7b50ad8b47b618db5006a89cdecd1d6e65f8b9f32b803947->leave($__internal_9f436fd90d4ddf6d7b50ad8b47b618db5006a89cdecd1d6e65f8b9f32b803947_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
", "@Framework/Form/password_widget.html.php", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/password_widget.html.php");
    }
}
