<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_cf81ec3092be0f0084facd9830a10688cc18b459082fade1c92fda720aeace07 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f83484c347e426d75f3ab99bb740ed7f75e703bbc26d6928030a1948309d96f1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f83484c347e426d75f3ab99bb740ed7f75e703bbc26d6928030a1948309d96f1->enter($__internal_f83484c347e426d75f3ab99bb740ed7f75e703bbc26d6928030a1948309d96f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        $__internal_821857dd5117cece1775d3dc4e3782084f050f9d01fd77f8919e5270ccd6a9de = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_821857dd5117cece1775d3dc4e3782084f050f9d01fd77f8919e5270ccd6a9de->enter($__internal_821857dd5117cece1775d3dc4e3782084f050f9d01fd77f8919e5270ccd6a9de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
";
        
        $__internal_f83484c347e426d75f3ab99bb740ed7f75e703bbc26d6928030a1948309d96f1->leave($__internal_f83484c347e426d75f3ab99bb740ed7f75e703bbc26d6928030a1948309d96f1_prof);

        
        $__internal_821857dd5117cece1775d3dc4e3782084f050f9d01fd77f8919e5270ccd6a9de->leave($__internal_821857dd5117cece1775d3dc4e3782084f050f9d01fd77f8919e5270ccd6a9de_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
", "@Framework/FormTable/button_row.html.php", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/button_row.html.php");
    }
}
