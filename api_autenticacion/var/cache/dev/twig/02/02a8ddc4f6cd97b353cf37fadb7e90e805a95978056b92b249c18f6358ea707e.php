<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_8ef342efd341b9db32993075d5f34bd1be00e59b5df52a881af64cf6a9c6b9dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e0723189f1fec6631cc4410e8420d1433e4cb3f29e87c4800f1de155ddc5c8c1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e0723189f1fec6631cc4410e8420d1433e4cb3f29e87c4800f1de155ddc5c8c1->enter($__internal_e0723189f1fec6631cc4410e8420d1433e4cb3f29e87c4800f1de155ddc5c8c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        $__internal_ff0c651760cacd3a0c723d8315e101dd20a57539e2696f4834030c9039cd0b12 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ff0c651760cacd3a0c723d8315e101dd20a57539e2696f4834030c9039cd0b12->enter($__internal_ff0c651760cacd3a0c723d8315e101dd20a57539e2696f4834030c9039cd0b12_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
*/
";
        
        $__internal_e0723189f1fec6631cc4410e8420d1433e4cb3f29e87c4800f1de155ddc5c8c1->leave($__internal_e0723189f1fec6631cc4410e8420d1433e4cb3f29e87c4800f1de155ddc5c8c1_prof);

        
        $__internal_ff0c651760cacd3a0c723d8315e101dd20a57539e2696f4834030c9039cd0b12->leave($__internal_ff0c651760cacd3a0c723d8315e101dd20a57539e2696f4834030c9039cd0b12_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "TwigBundle:Exception:exception.js.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.js.twig");
    }
}
