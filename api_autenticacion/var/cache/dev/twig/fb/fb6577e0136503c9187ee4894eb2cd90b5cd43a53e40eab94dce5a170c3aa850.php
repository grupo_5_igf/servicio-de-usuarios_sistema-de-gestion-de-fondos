<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_dc877f7ddd8612200a865db254dea07957d021e8dc53252cc566674c569806b6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1c2294a463ee08c370fd81447197f5aa012cda0d7e9ec33d10c9090a29799a3a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1c2294a463ee08c370fd81447197f5aa012cda0d7e9ec33d10c9090a29799a3a->enter($__internal_1c2294a463ee08c370fd81447197f5aa012cda0d7e9ec33d10c9090a29799a3a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        $__internal_a4a71e9a140bfb598c47df309eb08d9b78e63e4b810cf2d8403c9b22332bf15f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a4a71e9a140bfb598c47df309eb08d9b78e63e4b810cf2d8403c9b22332bf15f->enter($__internal_a4a71e9a140bfb598c47df309eb08d9b78e63e4b810cf2d8403c9b22332bf15f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_1c2294a463ee08c370fd81447197f5aa012cda0d7e9ec33d10c9090a29799a3a->leave($__internal_1c2294a463ee08c370fd81447197f5aa012cda0d7e9ec33d10c9090a29799a3a_prof);

        
        $__internal_a4a71e9a140bfb598c47df309eb08d9b78e63e4b810cf2d8403c9b22332bf15f->leave($__internal_a4a71e9a140bfb598c47df309eb08d9b78e63e4b810cf2d8403c9b22332bf15f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/form_row.html.php", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_row.html.php");
    }
}
