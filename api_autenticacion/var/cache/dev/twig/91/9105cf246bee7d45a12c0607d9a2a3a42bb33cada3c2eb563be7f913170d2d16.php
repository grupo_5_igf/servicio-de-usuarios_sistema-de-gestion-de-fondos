<?php

/* :usuario:edit.html.twig */
class __TwigTemplate_c4e015624eccbbd742a1911ddab74144dc5aa7bfc4857a3ab7de29f7fb065f31 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":usuario:edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1db99a1627d4d1d47326b06ec8f88020a2c8dbc8a32663406389d23d2ac59a5b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1db99a1627d4d1d47326b06ec8f88020a2c8dbc8a32663406389d23d2ac59a5b->enter($__internal_1db99a1627d4d1d47326b06ec8f88020a2c8dbc8a32663406389d23d2ac59a5b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":usuario:edit.html.twig"));

        $__internal_22fae405b5c2ee53be38525340c2de153e123a230f394d98a6206f39d343c63a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_22fae405b5c2ee53be38525340c2de153e123a230f394d98a6206f39d343c63a->enter($__internal_22fae405b5c2ee53be38525340c2de153e123a230f394d98a6206f39d343c63a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":usuario:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1db99a1627d4d1d47326b06ec8f88020a2c8dbc8a32663406389d23d2ac59a5b->leave($__internal_1db99a1627d4d1d47326b06ec8f88020a2c8dbc8a32663406389d23d2ac59a5b_prof);

        
        $__internal_22fae405b5c2ee53be38525340c2de153e123a230f394d98a6206f39d343c63a->leave($__internal_22fae405b5c2ee53be38525340c2de153e123a230f394d98a6206f39d343c63a_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_549cb8e489683f1273a5e57f06623ff302a6b45bb7a3207525b80a2ddd5695e6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_549cb8e489683f1273a5e57f06623ff302a6b45bb7a3207525b80a2ddd5695e6->enter($__internal_549cb8e489683f1273a5e57f06623ff302a6b45bb7a3207525b80a2ddd5695e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_2034d898da429a9076bf233a05b53c346ce418c4c1cf271dd1921c6adc5d1e3d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2034d898da429a9076bf233a05b53c346ce418c4c1cf271dd1921c6adc5d1e3d->enter($__internal_2034d898da429a9076bf233a05b53c346ce418c4c1cf271dd1921c6adc5d1e3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Usuario edit</h1>

    ";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Edit\" />
    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("usuario_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            ";
        // line 16
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 18
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_2034d898da429a9076bf233a05b53c346ce418c4c1cf271dd1921c6adc5d1e3d->leave($__internal_2034d898da429a9076bf233a05b53c346ce418c4c1cf271dd1921c6adc5d1e3d_prof);

        
        $__internal_549cb8e489683f1273a5e57f06623ff302a6b45bb7a3207525b80a2ddd5695e6->leave($__internal_549cb8e489683f1273a5e57f06623ff302a6b45bb7a3207525b80a2ddd5695e6_prof);

    }

    public function getTemplateName()
    {
        return ":usuario:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 18,  75 => 16,  69 => 13,  62 => 9,  57 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Usuario edit</h1>

    {{ form_start(edit_form) }}
        {{ form_widget(edit_form) }}
        <input type=\"submit\" value=\"Edit\" />
    {{ form_end(edit_form) }}

    <ul>
        <li>
            <a href=\"{{ path('usuario_index') }}\">Back to the list</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", ":usuario:edit.html.twig", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/app/Resources/views/usuario/edit.html.twig");
    }
}
