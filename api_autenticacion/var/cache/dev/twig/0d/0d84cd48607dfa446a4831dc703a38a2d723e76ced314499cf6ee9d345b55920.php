<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_2dfd8aa918acdb0aa52d5b841fa5ef084d9174251fd0f26610a58ce9eca8d635 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_373106f6771ca006097536c48d52a8f7207f20986ca7460383116c12fea56081 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_373106f6771ca006097536c48d52a8f7207f20986ca7460383116c12fea56081->enter($__internal_373106f6771ca006097536c48d52a8f7207f20986ca7460383116c12fea56081_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        $__internal_a7a11634ee9437616d833f9f3c240bc61c7e0236d348e453ee964cb2f6ce7741 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a7a11634ee9437616d833f9f3c240bc61c7e0236d348e453ee964cb2f6ce7741->enter($__internal_a7a11634ee9437616d833f9f3c240bc61c7e0236d348e453ee964cb2f6ce7741_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_373106f6771ca006097536c48d52a8f7207f20986ca7460383116c12fea56081->leave($__internal_373106f6771ca006097536c48d52a8f7207f20986ca7460383116c12fea56081_prof);

        
        $__internal_a7a11634ee9437616d833f9f3c240bc61c7e0236d348e453ee964cb2f6ce7741->leave($__internal_a7a11634ee9437616d833f9f3c240bc61c7e0236d348e453ee964cb2f6ce7741_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
", "@Framework/Form/reset_widget.html.php", "/home/carlos/servicio-de-usuarios_sistema-de-gestion-de-fondos/api_autenticacion/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/reset_widget.html.php");
    }
}
